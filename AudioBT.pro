QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = AudioBT
TEMPLATE = app


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/include \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/include/client/ \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/modules/btpmhdsm \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/modules/btpmhdsm/client \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/Bluetopia/include \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/Bluetopia/btpskrnl/threaded/ \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/Bluetopia/btpscfg/ \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/btpmerr \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/btpmvs/wl18xx/ \
    /home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/Bluetopia/SBC/SS1SBC/

SOURCES += \
    Controller/appController.cpp \
    Model/appModel.cpp \
    Model/bluetoothManager.cpp \
    Model/bluetopia.cpp \
    View/appView.cpp \
    View/connDonePage.cpp \
    View/connectPage.cpp \
    View/pairingPage.cpp \
    View/welcomePage.cpp \
    main.cpp

HEADERS += \
    Controller/appController.h \
    Model/appModel.h \
    Model/bluetoothManager.h \
    Model/bluetopia.h \
    View/appView.h \
    View/connDonePage.h \
    View/connectPage.h \
    View/pairingPage.h \
    View/welcomePage.h

FORMS += \
    View/connect.ui \
    View/connectionDone.ui \
    View/pairing.ui \
    View/welcome.ui \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += \
    -L/home/dinex/Cdk/SKDinema.imx6/trunk/cdk/rootfs/usr/bin/BluetopiaPM/lib \
    -lBTPM_C -lBTPM_HAL_C \
    -lBtnsEventGen2 -lGlassesCheck -lTouchEventsGen2 -lVisionARAlertFactory \
    -lAbstractVisionARController -lAbstractVisionARModel -lAbstractVisionARView \
    -lThreadObjInterface -lBtnsInterface -lBatteryManager -lAlertsManager -lLauncherAdapterInterface -lTouchInterface

RESOURCES += \
    resource.qrc
