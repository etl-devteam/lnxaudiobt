#include <QApplication>
#include <QScopedPointer>
#include <qglobal.h>

#include "Controller/appController.h"


int main(int argc, char *argv[])
{
    const QString DEVICE_ADD = argc == 1 ? "EA0AEFFC3DB2" : argv[1];

    QScopedPointer<QApplication> a(new QApplication(argc, argv));

    AppController::startCheckingGlasses();

    QScopedPointer<AppModel> model(new AppModel(DEVICE_ADD));
    QScopedPointer<AppView>  view (new AppView);

    AppController controller(view.data(), model.data(), "AudioBT/AudioBT");

    return a->exec();
}
