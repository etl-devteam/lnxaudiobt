#ifndef PAIRINGPAGE_H
#define PAIRINGPAGE_H

#include "ui_pairing.h"


class PairingPage final
{
public:
    PairingPage();
    ~PairingPage();

    void setupUi(QWidget *widget);

private:
    Ui_Pairing_page *pairingPage;
};


#endif // PAIRINGPAGE_H
