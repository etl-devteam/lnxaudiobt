#ifndef APPVIEW_H
#define APPVIEW_H

#include <VisionAR/Framework/abstractVisionARView.h>

#include "welcomePage.h"
#include "connectPage.h"
#include "pairingPage.h"
#include "connDonePage.h"


class AppView final : public AbstractVisionARView
{
    Q_OBJECT

public:
    AppView();

private:
    QScopedPointer<WelcomePage>    welcomePage;
    QScopedPointer<ConnectionPage> connectionPage;
    QScopedPointer<PairingPage>    pairingPage;
    QScopedPointer<ConnDonePage>   connDonePage;

public slots:
};


#endif // APPVIEW_H
