#include "connectPage.h"


ConnectionPage::ConnectionPage() :
    connectionPage{ new Ui_Connection_page }
{
}


ConnectionPage::~ConnectionPage()
{
    delete connectionPage;
    connectionPage = nullptr;
}


void ConnectionPage::setupUi(QWidget *widget)
{
    connectionPage->setupUi(widget);
}
