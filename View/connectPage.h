#ifndef CONNECTPAGE_H
#define CONNECTPAGE_H

#include "ui_connect.h"


class ConnectionPage final
{
public:
    ConnectionPage();
    ~ConnectionPage();

    void setupUi(QWidget *widget);

private:
    Ui_Connection_page *connectionPage;
};


#endif // CONNECTPAGE_H
