#include "pairingPage.h"


PairingPage::PairingPage() :
    pairingPage{ new Ui_Pairing_page }
{
}


PairingPage::~PairingPage()
{
    delete pairingPage;
    pairingPage = nullptr;
}


void PairingPage::setupUi(QWidget *widget)
{
    pairingPage->setupUi(widget);
}
