#include "appView.h"


AppView::AppView() :
    AbstractVisionARView(),
    welcomePage    { new WelcomePage    },
    connectionPage { new ConnectionPage },
    pairingPage    { new PairingPage    },
    connDonePage   { new ConnDonePage   }
{
    // the order in which you "register window"s here
    // matches the number you pass in "showWindow(int)"

    registerWindow(welcomePage.data());
    registerWindow(connectionPage.data());
    registerWindow(pairingPage.data());
    registerWindow(connDonePage.data());
}
