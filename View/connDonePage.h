#ifndef CONNDONEPAGE_H
#define CONNDONEPAGE_H

#include "ui_connectionDone.h"


class ConnDonePage final
{
public:
    ConnDonePage();
    ~ConnDonePage();

    void setupUi(QWidget *widget);

private:
    Ui_ConnDone_page *connDonePage;
};


#endif // CONNDONEPAGE_H
