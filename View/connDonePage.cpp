#include "connDonePage.h"


ConnDonePage::ConnDonePage() :
    connDonePage{ new Ui_ConnDone_page }
{
}


ConnDonePage::~ConnDonePage()
{
    delete connDonePage;
    connDonePage = nullptr;
}


void ConnDonePage::setupUi(QWidget *widget)
{
    connDonePage->setupUi(widget);
}
