#include "appController.h"

#include <QDebug>


AppController::AppController(AppView *view, AppModel *model, QString app_relative_path) :
    AbstractVisionARController(view, model, app_relative_path)
{
}


void AppController::connections()
{
}


void AppController::endActions()
{
}
