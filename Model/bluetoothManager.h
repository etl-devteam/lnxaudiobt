#ifndef BLUETOOTHMANAGER_H
#define BLUETOOTHMANAGER_H

#include <QObject>


class BluetoothManager final : public QObject
{
    Q_OBJECT

public:
    BluetoothManager(const QString &deviceAdd);
    ~BluetoothManager();

    void QueryRemoteDeviceService();
    int  ConnectDevice();
    int  RingIndication();
    void SetSpeakerGain();
    void SetMicrophoneGain();
    int Write_SCO_Configuration();
    int ManageAudio(int Setup);
    void QueryHeadsetService();
    void QueryRemoteDeviceProperty();
    void Disconnect();

private:
    const QString deviceAdd;

private slots:
};


#endif // BLUETOOTHMANAGER_H
