#include <QProcess>
#include <qglobal.h>

#include "bluetoothManager.h"
#include "bluetopia.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif


BluetoothManager::BluetoothManager(const QString &deviceAdd) :
    QObject(),
    deviceAdd{ deviceAdd }
{
    Initialize(nullptr);
    EnableHDK(nullptr);
    SetDevicePower(nullptr);
    HDSRegisterDataCallback(nullptr);
    HDSRegisterEventCallback(nullptr);
    RegisterAuthentication(nullptr);
}


BluetoothManager::~BluetoothManager()
{
}


void BluetoothManager::QueryRemoteDeviceService()
{
    ParameterList_t params1, params2;

    params1.NumberofParameters = 2;
    params1.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());
    params1.Params[1].intParam = 1;

    params2.NumberofParameters = 3;
    params2.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());
    params2.Params[1].intParam = 0;
    params2.Params[2].intParam = 2000;

    QueryRemoteDeviceServices(&params1);
    sleep(2);
    QueryRemoteDeviceServices(&params2);
}


int BluetoothManager::ConnectDevice()
{
    ParameterList_t params;
    params.NumberofParameters = 1;
    params.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());

    return HDSMConnectDevice(&params);
}


int BluetoothManager::RingIndication()
{
    ParameterList_t params;
    params.NumberofParameters = 1;
    params.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());

    return HDSMRingIndication(&params);
}


void BluetoothManager::SetSpeakerGain()
{
    ParameterList_t params;
    params.NumberofParameters = 2;
    params.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());
    params.Params[1].intParam = 12;

    HDSMSetRemoteSpeakerGain(&params);
}


void BluetoothManager::SetMicrophoneGain()
{
    ParameterList_t params;
    params.NumberofParameters = 2;
    params.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());
    params.Params[1].intParam = 10;

    HDSMSetRemoteMicrophoneGain(&params);
}


int BluetoothManager::Write_SCO_Configuration()
{
    ParameterList_t params;
    params.NumberofParameters = 2;
    params.Params[0].intParam = 120;
    params.Params[1].intParam = 500;

    return write_SCO_Configuration(&params);
}


int BluetoothManager::ManageAudio(int Setup)
{
    ParameterList_t params;
    params.NumberofParameters = Setup == 0 ? 2 : 3;
    params.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());
    params.Params[1].intParam = Setup;
    if (Setup == 1) params.Params[2].intParam = 1;

    return ManageAudioConnection(&params);
}


void BluetoothManager::QueryHeadsetService()
{
    ParameterList_t params;
    params.NumberofParameters = 2;
    params.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());
    params.Params[1].intParam = 1;

    QueryHeadsetServices(&params);
}


void BluetoothManager::QueryRemoteDeviceProperty()
{
    ParameterList_t params;
    params.NumberofParameters = 1;
    params.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());

    QueryRemoteDeviceProperties(&params);
}


void BluetoothManager::Disconnect()
{
    ParameterList_t params;
    params.NumberofParameters = 1;
    params.Params[0].strParam = const_cast<char *>(deviceAdd.toStdString().c_str());

    HDSMDisconnectDevice(&params);
}
