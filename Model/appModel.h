#ifndef APPMODEL_H
#define APPMODEL_H

#include <VisionAR/Framework/abstractVisionARModel.h>

#include "bluetoothManager.h"


class AppModel final : public AbstractVisionARModel
{
    Q_OBJECT

public:
    AppModel(const QString &deviceAdd);
    ~AppModel();

private:

    enum : short {
        FIRST_WINDOW = 1, SECOND_WINDOW, THIRD_WINDOW, FOURTH_WINDOW,
    };

    enum class AppStatus : short {
        WELCOME, CONNECTION_INFO, PAIRING, CONNECTED,
    };

    AppStatus status;
    const QString deviceAdd;

    BluetoothManager *btManager;

    void runModel()    override;
    void backPressed() override {}
    void okPressed()   override;

    void doubleTap()                               override {}
    void swipeForward()                            override {}
    void swipeBackward()                           override {}
    void backReleased(    int /*timePressed*/)     override;
    void okReleased(      int /*timePressed*/ = 0) override {}
    void forwardReleased( int /*timePressed*/)     override {}
    void backwardReleased(int /*timePressed*/)     override {}
    void forwardPressed()                          override {}
    void backwardPressed()                         override {}
    void singleTap()                               override {}

    void manageAlertsEnd(QString id, bool value) override;

private slots:

signals:
};


#endif // APPMODEL_H
