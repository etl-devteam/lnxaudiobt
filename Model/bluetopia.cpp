#include "bluetopia.h"

#define VS_SCO_CONFIGURATION_COMMAND_ADDRESS          ((Word_t)(0xFE10))

#define VS_COMMAND_OGF(_CommandOpcode)                ((Byte_t)((_CommandOpcode) >> 10))
#define VS_COMMAND_OCF(_CommandOpcode)                ((Word_t)((_CommandOpcode) & (0x3FF)))


/* The EVM PCM is the clock master, Fsync @ 16KHz or 8KHz PCM @ 1.6MHz or 0.8MHz
 * (100 clock per fsync, 5 channels => 20PCM clock per channel)
 * PCM Slave=1,Frame Synch sample at rising edge = 0, Active low = 1 -> 1,0,1
 * 16bit channel = 16, with offset = 0, driven at rising edge = 0
 */
PCMCodecConfig_t PCMCodecConfig16k =
 {16000, 1600, 1, 0, 1, {16, 0, 0}, {16, 0, 0}, {16, 20, 0}, {16, 20, 0} };

/* The 8KHz (for CVSD) configuration. Just the Fsync frequency and PCM clock changed  */
PCMCodecConfig_t PCMCodecConfig8k =
 {8000, 800, 1, 0, 1, {16, 0, 0}, {16, 0, 0}, {16, 20, 0}, {16, 20, 0} };


/* Internal Variables to this Module (Remember that all variables    */
/* declared static are initialized to 0 automatically by the         */
/* compiler as part of standard C/C++).                              */
static Boolean_t           Initialized;          /* Variable which is used to hold  */
                                                 /* the current state of the        */
                                                 /* Bluetopia Platform Manager      */
                                                 /* Initialization.                 */

static unsigned int        DEVMCallbackID;       /* Variable which holds the        */
                                                 /* Callback ID of the currently    */
                                                 /* registered Device Manager       */
                                                 /* Callback ID.                    */

static unsigned int        AuthenticationCallbackID;/* Variable which holds the     */
                                                 /* current Authentication Callback */
                                                 /* ID that is assigned from the    */
                                                 /* Device Manager when the local   */
                                                 /* client registers for            */
                                                 /* Authentication.                 */

static unsigned int        HDSventCallbackID;    /* Variable which holds the        */
                                                 /* current HDS Event Callback ID   */
                                                 /* that is assigned from the HDS   */
                                                 /* Manager when the local client   */
                                                 /* registers for HDS Events.       */

static unsigned int        HDSDataCallbackID;    /* Variable which holds the        */
                                                 /* current HDS Data Callback ID    */
                                                 /* that is assigned from the HDS   */
                                                 /* Manager when the local client   */
                                                 /* registers for HDS Data Events.  */

static BD_ADDR_t           CurrentRemoteBD_ADDR; /* Variable which holds the        */
                                                 /* current BD_ADDR of the device   */
                                                 /* which is currently pairing or   */
                                                 /* authenticating.                 */

static GAP_IO_Capability_t IOCapability;         /* Variable which holds the        */
                                                 /* current I/O Capabilities that   */
                                                 /* are to be used for Secure Simple*/
                                                 /* Pairing.                        */

static Boolean_t           OOBSupport;           /* Variable which flags whether    */
                                                 /* or not Out of Band Secure Simple*/
                                                 /* Pairing exchange is supported.  */

static Boolean_t           MITMProtection;       /* Variable which flags whether or */
                                                 /* not Man in the Middle (MITM)    */
                                                 /* protection is to be requested   */
                                                 /* during a Secure Simple Pairing  */
                                                 /* procedure.                      */

static unsigned int        AudioDataCount;       /* Variable which is used to       */
                                                 /* control the rate at which Audio */
                                                 /* Data Event information is       */
                                                 /* printed to the console.         */

static Boolean_t        HDK_Platform = FALSE;    /* Variable flag which indicates   */
                                                 /* to this application that the    */
                                                 /* 18xx platform is connected to   */
                                                 /* HDK board with its own CODEC.   */

/* The following string table is used to map the API I/O Capabilities*/
/* values to an easily displayable string.                           */
static const char *IOCapabilitiesStrings[] =
{
    "Display Only",
    "Display Yes/No",
    "Keyboard Only",
    "No Input/Output"
};


/* The following function is responsible for Initializing the        */
/* Bluetopia Platform Manager Framework.  This function returns      */
/* zero if successful and a negative value if an error occurred.     */
int Initialize(ParameterList_t* /*TempParam*/)
{
   int Result;
   int ret_val;

   /* First, check to make sure that we are not already initialized.    */
   if(!Initialized)
   {    
     /* Now actually initialize the Platform Manager Service.       */
     Result = BTPM_Initialize((unsigned long)getpid(), NULL, ServerUnRegistrationCallback, NULL);

     if(!Result)
     {
        /* Initialization successful, go ahead and inform the user  */
        /* that it was successful and flag that the Platform Manager*/
        /* has been initialized.                                    */
        printf("BTPM_Initialize() Success: %d.\r\n", Result);

       if((Result = DEVM_RegisterEventCallback(DEVM_Event_Callback, NULL)) > 0)
       {
          printf("DEVM_RegisterEventCallback() Success: %d.\r\n", Result);

          /* Note the Callback ID and flag success.             */
          DEVMCallbackID = (unsigned int)Result;

          Initialized    = TRUE;

          ret_val        = 0;
       }
       else
       {
          /* Error registering the Callback, inform user and    */
          /* flag an error.                                     */
          printf("DEVM_RegisterEventCallback() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));

          ret_val = FUNCTION_ERROR;

          /* Since there was an error, go ahead and clean up the*/
          /* library.                                           */
          BTPM_Cleanup();
       }
     }
     else
     {
        /* Error initializing Platform Manager, inform the user.    */
        printf("BTPM_Initialize() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));

        ret_val = FUNCTION_ERROR;
     }
   }
   else
   {
      /* Already initialized, flag an error.                            */
      printf("Initialization Failure: Already initialized.\r\n");

      ret_val = FUNCTION_ERROR;
   }

   return(ret_val);
}


/* The following function Sets the HDK platform flag like the -k option at start */
int EnableHDK(ParameterList_t* /*TempParam*/)
{
    int                            ret_val = 0;

    /* First, check to make sure that we have already been initialized.  */
    if(Initialized)
    {
        /* Enable the HDK platform flag */
        HDK_Platform = TRUE;
        printf("\r\n HDK Platform flag is on \r\n");
    }
    else
    {
        /* Not initialized, flag an error.                                */
        printf("Platform Manager has not been initialized.\r\n");
        ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
    }

    return(ret_val);
}


/* The following function is responsible for Setting the Device Power*/
   /* of the Local Device.  This function returns zero if successful and*/
   /* a negative value if an error occurred.                            */
int SetDevicePower(ParameterList_t* /*TempParam*/)
{
   int Result;
   int ret_val;

   /* First, check to make sure that we are not already initialized.    */
   if(Initialized)
   {
     /* Now actually Perform the command.                           */
     /*
     if(TempParam->Params[0].intParam)
        Result = DEVM_PowerOnDevice();
     else
        Result = DEVM_PowerOffDevice();
     */

     Result = DEVM_PowerOnDevice();

     if(!Result)
     {
        /* Device Power request was successful, go ahead and inform */
        /* the User.                                                */
        printf("DEVM_Power%sDevice() Success: %d.\r\n", "On", Result);

        /* Return success to the caller.                            */
        ret_val = 0;

        /* Route the platform Audio after Bluetooth device power on */
        /*
        if(TempParam->Params[0].intParam) {
            RouteAudio();
        }
        */
        RouteAudio();
     }
     else
     {
        /* Error Powering On/Off the device, inform the user.       */
        printf("DEVM_Power%sDevice() Failure: %d, %s.\r\n", "On", Result, ERR_ConvertErrorCodeToString(Result));

        ret_val = FUNCTION_ERROR;
     }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = FUNCTION_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for registering with the HDS*/
   /* Manager to receive HDS Data Events (and to allow the local client */
   /* the ability to send SCO audio data).  This function returns zero  */
   /* if successful and a negative value if an error occurred.          */
int HDSRegisterDataCallback(ParameterList_t* /*TempParam*/)
{
   int Result;
   int ret_val;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* If there is an Data Callback Registered, then we need to flag  */
      /* an error.                                                      */
      if(!HDSDataCallbackID)
      {
         /* Callback has not been registered, go ahead and attempt to   */
         /* register it.                                                */
         Result = HDSM_Register_Data_Event_Callback(sctAudioGateway, HDSM_Event_Callback, NULL);
         if(Result > 0)
         {
            printf("HDSM_Register_Data_Event_Callback() Success: %d.\r\n", Result);

            /* Note the Callback ID and flag success.                   */
            HDSDataCallbackID = (unsigned int)Result;

            ret_val           = 0;
         }
         else
         {
            /* Error registering the Callback, inform user and flag an  */
            /* error.                                                   */
            printf("HDSM_Register_Data_Event_Callback() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));

            ret_val = FUNCTION_ERROR;
         }
      }
      else
      {
         /* Callback already registered, go ahead and notify the user.  */
         printf("HDS Manager Data Callback already registered.\r\n");

         ret_val = FUNCTION_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for registering with the HDS*/
   /* Manager to receive HDS Events.  This function returns zero if     */
   /* successful and a negative value if an error occurred.             */
int HDSRegisterEventCallback(ParameterList_t* /*TempParam*/)
{
   int Result;
   int ret_val;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* If there is an Event Callback Registered, then we need to flag */
      /* an error.                                                      */
      if(!HDSventCallbackID)
      {
        /* Callback has not been registered, go ahead and attempt to*/
        /* register it.                                             */
        Result = HDSM_Register_Event_Callback(sctAudioGateway, TRUE, HDSM_Event_Callback, NULL);
        if(Result > 0)
        {
           printf("HDSM_Register_Event_Callback() Success: %d.\r\n", Result);

           /* Note the Callback ID and flag success.                */
           HDSventCallbackID = (unsigned int)Result;

           ret_val            = 0;
        }
        else
        {
           /* Error registering the Callback, inform user and flag  */
           /* an error.                                             */
           printf("HDSM_Register_Event_Callback() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));

           ret_val = FUNCTION_ERROR;
        }
      }
      else
      {
         /* Callback already registered, go ahead and notify the user.  */
         printf("HDS Manager Event Callback already registered.\r\n");

         ret_val = FUNCTION_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for Registering the Local   */
/* Client to receive (and process) Authentication Events.  This      */
/* function returns zero if successful and a negative value if an    */
/* error occurred.                                                   */
int RegisterAuthentication(ParameterList_t* /*TempParam*/)
{
    int Result;
    int ret_val;

    /* First, check to make sure that we have already been initialized.  */
    if(Initialized)
    {
       /* Initialized, go ahead and attempt to Register for              */
       /* Authentication.                                                */
       if((Result = DEVM_RegisterAuthentication(DEVM_Authentication_Callback, NULL)) >= 0)
       {
          printf("DEVM_RegisterAuthentication() Success: %d.\r\n", Result);

          /* Note the Authentication Callback ID.                        */
          AuthenticationCallbackID = (unsigned int)Result;

          /* Flag success.                                               */
          ret_val                  = 0;
       }
       else
       {
          /* Error Registering for Authentication, inform the user and   */
          /* flag an error.                                              */
          printf("DEVM_RegisterAuthentication() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));

          ret_val = FUNCTION_ERROR;
       }
    }
    else
    {
       /* Not initialized, flag an error.                                */
       printf("Platform Manager has not been initialized.\r\n");

       ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
    }

    return(ret_val);
}


/* The following function is responsible for Querying the Remote     */
   /* Device Services of the specified Remote Device.  This function    */
   /* returns zero if successful and a negative value if an error       */
   /* occurred.                                                         */
int QueryRemoteDeviceServices(ParameterList_t *TempParam)
{
   int                     Result;
   int                     ret_val;
   BD_ADDR_t               BD_ADDR;
   unsigned int            TotalServiceSize;
   unsigned char          *ServiceData;
   DEVM_Parsed_SDP_Data_t  ParsedSDPData;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* Make sure that all of the parameters required for this function*/
      /* appear to be at least semi-valid.                              */
      if((TempParam) && (TempParam->NumberofParameters > 1))
      {
         /* Initialize success.                                         */
         ret_val = 0;

         /* Convert the parameter to a Bluetooth Device Address.        */
         StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

         printf("Attempting Query Remote Device %s For Services.\r\n", TempParam->Params[0].strParam);

         if(!TempParam->Params[1].intParam)
         {
            /* Caller has requested to actually retrieve it locally,    */
            /* determine how many bytes were requested.                 */
            if(TempParam->NumberofParameters > 2)
               ServiceData = (unsigned char *)BTPS_AllocateMemory(TempParam->Params[2].intParam);
            else
               ret_val = INVALID_PARAMETERS_ERROR;
         }
         else
            ServiceData = NULL;

         if(!ret_val)
         {
            if((TempParam->Params[1].intParam) || ((!TempParam->Params[1].intParam) && (ServiceData)))
            {
               if((Result = DEVM_QueryRemoteDeviceServices(BD_ADDR, (Boolean_t)TempParam->Params[1].intParam, TempParam->Params[1].intParam?0:TempParam->Params[2].intParam, ServiceData, &TotalServiceSize)) >= 0)
               {
                  printf("DEVM_QueryRemoteDeviceServices() Success: %d, Total Number Service Bytes: %d.\r\n", Result, (TempParam->Params[1].intParam)?0:TotalServiceSize);

                  /* Now convert the Raw Data to parsed data.           */
                  if(Result)
                  {
                     Result = DEVM_ConvertRawSDPStreamToParsedSDPData(Result, ServiceData, &ParsedSDPData);

                     if(!Result)
                     {
                        /* Success, Display the Parsed Data.            */
                        DisplayParsedServiceData(&ParsedSDPData);

                        /* All finished with the parsed data, so free   */
                        /* it.                                          */
                        DEVM_FreeParsedSDPData(&ParsedSDPData);
                     }
                     else
                        printf("DEVM_ConvertRawSDPStreamToParsedSDPData() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));
                  }

                  /* Flag success.                                      */
                  ret_val = 0;
               }
               else
               {
                  /* Error attempting to query Services, inform the user*/
                  /* and flag an error.                                 */
                  printf("DEVM_QueryRemoteDeviceServices() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));

                  ret_val = FUNCTION_ERROR;
               }

               /* Free any memory that was allocated.                   */
               if(ServiceData)
                  BTPS_FreeMemory(ServiceData);
            }
            else
            {
               /* Unable to allocate memory for List.                   */
               printf("Unable to allocate memory for %d Service Bytes.\r\n", TempParam->Params[2].intParam);

               ret_val = FUNCTION_ERROR;
            }
         }
         else
         {
            /* One or more of the necessary parameters is/are invalid.  */
            printf("Usage: QueryRemoteDeviceServices [BD_ADDR] [Force Update] [Bytes to Query (specified if Force is 0)].\r\n");

            ret_val = INVALID_PARAMETERS_ERROR;
         }
      }
      else
      {
         /* One or more of the necessary parameters is/are invalid.     */
         printf("Usage: QueryRemoteDeviceServices [BD_ADDR] [Force Update] [Bytes to Query (specified if Force is 0)].\r\n");

         ret_val = INVALID_PARAMETERS_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for establishing a Headset  */
   /* connection.  This function returns zero on successful execution   */
   /* and a negative value on all errors.                               */
int HDSMConnectDevice(ParameterList_t *TempParam)
{
   int           Result;
   int           ret_val;
   BD_ADDR_t     BD_ADDR;
   unsigned int  ServerPortNumber;
   unsigned long ConnectionFlags;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* Verify that the HDS Event Callback has been registered.        */
      if(HDSventCallbackID)
      {
         /* The port ID appears to be at least semi-valid, now check to */
         /* make sure the passed in parameters appears to be semi-valid.*/
         if((TempParam) && (TempParam->NumberofParameters > 0))
         {
            /* Convert the parameter to a Bluetooth Device Address.     */
            StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

            /* Determine if a port number was specified, if one is not  */
            /* specified we will query the device manager.              */
            if((TempParam->NumberofParameters >= 2) && (TempParam->Params[1].intParam))
            {
               ServerPortNumber = TempParam->Params[1].intParam;

               Result           = 0;
            }
            else
            {
               /* Port number was not specified so query the Port Number*/
               /* for the specified BD_ADDR.                            */
               Result = GetHeadsetServiceInformation(BD_ADDR, FALSE, &ServerPortNumber);
            }

            if(!Result)
            {
               /* Determine if Connection Flags were specified.         */
               if(TempParam->NumberofParameters >= 3)
                  ConnectionFlags = (unsigned long)TempParam->Params[2].intParam;
               else
                  ConnectionFlags = 0;

               /* Call the function to connect the remote device.       */
               Result = HDSM_Connect_Remote_Device(sctAudioGateway, BD_ADDR, ServerPortNumber, ConnectionFlags, HDSM_Event_Callback, NULL, NULL);

               /* Set the return value of this function equal to the    */
               /* Result of the function call.                          */
               ret_val = Result;

               if(!Result)
               {
                  /* The function was submitted successfully.           */
                  printf("HDSM_Connect_Remote_Device: Function Successful.\r\n");
               }
               else
               {
                  /* There was an error submitting the function.        */
                  printf("HDSM_Connect_Remote_Device() Failure: %d (%s).\r\n", Result, ERR_ConvertErrorCodeToString(Result));
               }
            }
            else
            {
               printf("Failed to find the Port Number on the specified remote device. (Did you do SDP)?\r\n");

               /* Set the return value of this function equal to the    */
               /* Result of the function call.                          */
               ret_val = Result;
            }
         }
         else
         {
            /* One or more of the necessary parameters is/are invalid.  */
            printf("Usage: Connect [BD_ADDR] [Port Number (optional)] [Connection Flags (optional - 0 = No Flags, 1 = Authentication, 2 = Encryption, 3 = Authentication/Encryption)].\r\n");

            ret_val = INVALID_PARAMETERS_ERROR;
         }
      }
      else
      {
         /* One or more of the necessary parameters is/are invalid.     */
         printf("HDS Event Callback MUST be registered before making this call.\r\n");

         ret_val = INVALID_PARAMETERS_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for sending a Ring          */
/* Indication to a Remote Device.  This function returns zero on     */
/* successful execution and a negative value on all errors.          */
int HDSMRingIndication(ParameterList_t *TempParam)
{
    int       Result;
    int       ret_val;
    BD_ADDR_t BD_ADDR;

    /* First, check to make sure that we have already been initialized.  */
    if(Initialized)
    {
       /* Verify that the HDS Event Callback has been registered.        */
       if(HDSventCallbackID)
       {
          /* make sure the passed in parameters appears to be semi-valid.*/
          if((TempParam) && (TempParam->NumberofParameters > 0) && (TempParam->Params[0].strParam))
          {
             /* Convert the parameter to a Bluetooth Device Address.     */
             StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

             /* The Port ID appears to be is a semi-valid value.  Now    */
             /* submit the command.                                      */
             Result = HDSM_Ring_Indication(HDSventCallbackID, BD_ADDR);

             /* Set the return value of this function equal to the Result*/
             /* of the function call.                                    */
             ret_val = Result;

             /* Now check to see if the command was submitted            */
             /* successfully.                                            */
             if(!Result)
             {
                /* The function was submitted successfully.              */
                printf("HDSM_Ring_Indication: Function Successful.\r\n");
             }
             else
             {
                /* There was an error submitting the function.           */
                printf("HDSM_Ring_Indication() Failure: %d (%s).\r\n", Result, ERR_ConvertErrorCodeToString(Result));
             }
          }
          else
          {
             /* One or more of the parameters are invalid.               */
             printf("Usage: RingIndication [BD_ADDR].\r\n");

             ret_val = INVALID_PARAMETERS_ERROR;
          }
       }
       else
       {
          /* One or more of the necessary parameters is/are invalid.     */
          printf("HDS Event Callback MUST be registered before making this call.\r\n");

          ret_val = INVALID_PARAMETERS_ERROR;
       }
    }
    else
    {
       /* Not initialized, flag an error.                                */
       printf("Platform Manager has not been initialized.\r\n");

       ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
    }

    return(ret_val);
}


/* The following function is the Callback function that is installed */
/* to be notified when any local IPC connection to the server has    */
/* been lost.  This case only occurs when the Server exits.  This    */
/* callback allows the application mechanism to be notified so that  */
/* all resources can be cleaned up (i.e.  call BTPM_Cleanup().       */
void BTPSAPI ServerUnRegistrationCallback(void */*CallbackParameter*/)
{
   printf("Server has been Un-Registered, Everything must be re-initialized.\r\n");

   printf("Headset>");

   /* Clean up everything.                                              */
   Cleanup(NULL);

   /* Make sure the output is displayed to the user.                    */
   fflush(stdout);
}


/* The following function is the Device Manager Event Callback       */
/* function that is Registered with the Device Manager.  This        */
/* callback is responsible for processing all Device Manager Events. */
void BTPSAPI DEVM_Event_Callback(DEVM_Event_Data_t *EventData, void */*CallbackParameter*/)
{
   char Buffer[32];

   if(EventData)
   {
      printf("\r\n");

      switch(EventData->EventType)
      {
         case detDevicePoweredOn:
            printf("Device Powered On.\r\n");
            break;
         case detDevicePoweringOff:
            printf("Device Powering Off Event, Timeout: 0x%08X.\r\n", EventData->EventData.DevicePoweringOffEventData.PoweringOffTimeout);
            break;
         case detDevicePoweredOff:
            printf("Device Powered Off.\r\n");
            break;
         case detLocalDevicePropertiesChanged:
            printf("Local Device Properties Changed.\r\n");

            DisplayLocalDeviceProperties(EventData->EventData.LocalDevicePropertiesChangedEventData.ChangedMemberMask, &(EventData->EventData.LocalDevicePropertiesChangedEventData.LocalDeviceProperties));
            break;
         case detDeviceDiscoveryStarted:
            printf("Device Discovery Started.\r\n");
            break;
         case detDeviceDiscoveryStopped:
            printf("Device Discovery Stopped.\r\n");
            break;
         case detRemoteDeviceFound:
            printf("Remote Device Found.\r\n");

            DisplayRemoteDeviceProperties(0, &(EventData->EventData.RemoteDeviceFoundEventData.RemoteDeviceProperties));
            break;
         case detRemoteDeviceDeleted:
            BD_ADDRToStr(EventData->EventData.RemoteDeviceDeletedEventData.RemoteDeviceAddress, Buffer);

            printf("Remote Device Deleted: %s.\r\n", Buffer);
            break;
         case detRemoteDevicePropertiesChanged:
            printf("Remote Device Properties Changed.\r\n");

            DisplayRemoteDeviceProperties(EventData->EventData.RemoteDevicePropertiesChangedEventData.ChangedMemberMask, &(EventData->EventData.RemoteDevicePropertiesChangedEventData.RemoteDeviceProperties));
            break;
         case detRemoteDevicePropertiesStatus:
            BD_ADDRToStr(EventData->EventData.RemoteDevicePropertiesStatusEventData.RemoteDeviceProperties.BD_ADDR, Buffer);

            printf("Remote Device Properties Status: %s, %s.\r\n", Buffer, EventData->EventData.RemoteDevicePropertiesStatusEventData.Success?"SUCCESS":"FAILURE");

            DisplayRemoteDeviceProperties(0, &(EventData->EventData.RemoteDevicePropertiesStatusEventData.RemoteDeviceProperties));
            break;
         case detRemoteDeviceServicesStatus:
            BD_ADDRToStr(EventData->EventData.RemoteDeviceServicesStatusEventData.RemoteDeviceAddress, Buffer);

            printf("Remote Device %s Services Status: %s, %s.\r\n", Buffer, (EventData->EventData.RemoteDeviceServicesStatusEventData.StatusFlags & DEVM_REMOTE_DEVICE_SERVICES_STATUS_FLAGS_LOW_ENERGY)?"LE":"BR/EDR", (EventData->EventData.RemoteDeviceServicesStatusEventData.StatusFlags & DEVM_REMOTE_DEVICE_SERVICES_STATUS_FLAGS_SUCCESS)?"SUCCESS":"FAILURE");
            break;
         case detRemoteDevicePairingStatus:
            BD_ADDRToStr(EventData->EventData.RemoteDevicePairingStatusEventData.RemoteDeviceAddress, Buffer);

            printf("Remote Device Pairing Status: %s, %s (0x%02X)\r\n", Buffer, (EventData->EventData.RemoteDevicePairingStatusEventData.Success)?"SUCCESS":"FAILURE", EventData->EventData.RemoteDevicePairingStatusEventData.AuthenticationStatus);
            break;
         case detRemoteDeviceAuthenticationStatus:
            BD_ADDRToStr(EventData->EventData.RemoteDeviceAuthenticationStatusEventData.RemoteDeviceAddress, Buffer);

            printf("Remote Device Authentication Status: %s, %d (%s)\r\n", Buffer, EventData->EventData.RemoteDeviceAuthenticationStatusEventData.Status, (EventData->EventData.RemoteDeviceAuthenticationStatusEventData.Status)?ERR_ConvertErrorCodeToString(EventData->EventData.RemoteDeviceAuthenticationStatusEventData.Status):"SUCCESS");
            break;
         case detRemoteDeviceEncryptionStatus:
            BD_ADDRToStr(EventData->EventData.RemoteDeviceEncryptionStatusEventData.RemoteDeviceAddress, Buffer);

            printf("Remote Device Encryption Status: %s, %d (%s)\r\n", Buffer, EventData->EventData.RemoteDeviceEncryptionStatusEventData.Status, (EventData->EventData.RemoteDeviceEncryptionStatusEventData.Status)?ERR_ConvertErrorCodeToString(EventData->EventData.RemoteDeviceEncryptionStatusEventData.Status):"SUCCESS");
            break;
         case detRemoteDeviceConnectionStatus:
            BD_ADDRToStr(EventData->EventData.RemoteDeviceConnectionStatusEventData.RemoteDeviceAddress, Buffer);

            printf("Remote Device Connection Status: %s, %d (%s)\r\n", Buffer, EventData->EventData.RemoteDeviceConnectionStatusEventData.Status, (EventData->EventData.RemoteDeviceConnectionStatusEventData.Status)?ERR_ConvertErrorCodeToString(EventData->EventData.RemoteDeviceConnectionStatusEventData.Status):"SUCCESS");
            break;
         default:
            printf("Unknown Device Manager Event Received: 0x%08X, Length: 0x%08X.\r\n", (unsigned int)EventData->EventType, EventData->EventLength);
            break;
      }
   }
   else
      printf("\r\nDEVM Event Data is NULL.\r\n");

   printf("Headset>");

   /* Make sure the output is displayed to the user.                    */
   fflush(stdout);
}


/* The following function routes the platform Audio according the compiled device name */
void RouteAudio(void)
{
    printf(" Route Audio");
    /* Route the Audio according the HW development board. The MIC jack audio PCM *
     * data from the on-board CODEC is routed to the Bluetooth device PCM lines.  *
     * The Bluetooth PCM out is routed to the on-board CODEC also for Audio out   *
     * the Speakers jack.														  */

    /* The HDK Platform has on-board CODEC and Audio in/out Jacks.            	  */
    if(!HDK_Platform) {

#ifdef wl18xx
        /* Send Vendor Specific command to the Wilink Bluetooth FW. Configure the PCM   *
        *  interface parameters to: 16KHz sample rate, PCM clock rate 1.6MHz, PCM slave *
        *  FSynch- 1 clock with inverted polarity, 16 bit sample with no offset. The    *
        *  WL18xx Bluetooth PCM has 8/16 and 16/8 sample rate converted. The PCM  is    *
        *  allays on 16KHz.	Route the PCM lines with the GStreamer to 16KHz             */
        printf(" Route wl18xx 16K ");
        (void) PM_VS_PCM_Codec_Config(&PCMCodecConfig16k);
        RouteGStreamer(SAMPLE_RATE_16KHZ);
#endif

#ifdef cc256x
        /* Send Vendor Specific command to the CC256x Bluetooth FW. Configure the PCM   *
        *  interface parameters to: 8KHz sample rate, PCM clock rate 0.8MHz, PCM slave  *
        *  FSynch- 1 clock with inverted polarity, 16 bit sample with no offset. Route  *
        *  the PCM lines with the GStreamer to 8KHz. 8KHz chosen as default, updates    *
        *  according the CODEC selection HandsFree profile command.                 	*/
        printf(" Route cc256x 8K ");
        (void) PM_VS_PCM_Codec_Config(&PCMCodecConfig8k);
        RouteGStreamer(SAMPLE_RATE_8KHZ);
#endif

        } else {
            printf("No Audio initialize for HDK, WiLink Platform. ");
        }
}


/* The following function is for an HDS Event Callback.  This        */
   /* function will be called whenever a HDS Event occurs that is       */
   /* associated with the Bluetooth Stack.  This function passes to the */
   /* caller the HDS Event Data that occurred and the HDS Event Callback*/
   /* Parameter that was specified when this Callback was installed.    */
   /* The caller is free to use the contents of the HDS Event Data ONLY */
   /* in the context of this callback.  If the caller requires the Data */
   /* for a longer period of time, then the callback function MUST copy */
   /* the data into another Data Buffer.  This function is guaranteed   */
   /* NOT to be invoked more than once simultaneously for the specified */
   /* installed callback (i.e.  this function DOES NOT have be          */
   /* reentrant).  It Needs to be noted however, that if the same       */
   /* Callback is installed more than once, then the callbacks will be  */
   /* called serially.  Because of this, the processing in this function*/
   /* should be as efficient as possible.  It should also be noted that */
   /* this function is called in the Thread Context of a Thread that the*/
   /* User does NOT own.  Therefore, processing in this function should */
   /* be as efficient as possible (this argument holds anyway because   */
   /* another HDS Event will not be processed while this function call  */
   /* is outstanding).                                                  */
   /* * NOTE * This function MUST NOT Block and wait for events that can*/
   /*          only be satisfied by Receiving SPP Event Packets.  A     */
   /*          Deadlock WILL occur because NO SPP Event Callbacks will  */
   /*          be issued while this function is currently outstanding.  */
void BTPSAPI HDSM_Event_Callback(HDSM_Event_Data_t *EventData, void */*CallbackParameter*/)
{
   char                BoardStr[13];
   ConnectionTypeStr_t ConnectionStr;

   /* First, check to see if the required parameters appear to be       */
   /* semi-valid.                                                       */
   if(EventData != NULL)
   {
      if(EventData->EventType != hetHDSAudioData)
         printf("\r\n");

      /* The parameters appear to be semi-valid, now check to see what  */
      /* type the incoming event is.                                    */
      switch(EventData->EventType)
      {
         case hetHDSIncomingConnectionRequest:
            /* A Client is attempting to connect to the Server, display */
            /* the BD_ADDR of the connecting device.                    */
            BD_ADDRToStr(EventData->EventData.IncomingConnectionRequestEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.IncomingConnectionRequestEventData.ConnectionType, ConnectionStr);

            printf("hetHDSIncomingConnectionRequest, BD_ADDR: %s, Type: %s.\r\n", BoardStr, ConnectionStr);
            printf("Respond with the command: ConnectionRequestResponse\r\n");
            break;
         case hetHDSConnected:
            /* A Client has connected to the Server, display the BD_ADDR*/
            /* of the connecting device.                                */
            BD_ADDRToStr(EventData->EventData.ConnectedEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.ConnectedEventData.ConnectionType, ConnectionStr);
            printf("hetHDSConnected, BD_ADDR: %s, Type: %s.\r\n", BoardStr, ConnectionStr);
            break;
         case hetHDSDisconnected:
            /* A Client has connected to the Server, display the BD_ADDR*/
            /* of the connecting device.                                */
            BD_ADDRToStr(EventData->EventData.DisconnectedEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.DisconnectedEventData.ConnectionType, ConnectionStr);
            printf("hetHDSDisconnected, BD_ADDR: %s, Type: %s, Reason: %u.\r\n", BoardStr, ConnectionStr, EventData->EventData.DisconnectedEventData.DisconnectReason);
            break;
         case hetHDSConnectionStatus:
            /* The Client received a Connect Confirmation, display      */
            /* relevant information.                                    */
            BD_ADDRToStr(EventData->EventData.ConnectionStatusEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.ConnectionStatusEventData.ConnectionType, ConnectionStr);
            printf("hetHDSConnectionStatus, BD_ADDR: %s, Type: %s, Status: %u.\r\n", BoardStr, ConnectionStr, EventData->EventData.ConnectionStatusEventData.ConnectionStatus);
            break;
         case hetHDSAudioConnected:
            /* A Client has connected to the Server, display the BD_ADDR*/
            /* of the connecting device.                                */
            BD_ADDRToStr(EventData->EventData.AudioConnectedEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.AudioConnectedEventData.ConnectionType, ConnectionStr);
            printf("hetHDSAudioConnected, BD_ADDR: %s, Type: %s.\r\n", BoardStr, ConnectionStr);

            /* Flag that we would like the Audio Data Indication message*/
            /* to be displayed on the first reception of Audio Data.    */
            AudioDataCount = 1;
            break;
         case hetHDSAudioDisconnected:
            /* A Client has connected to the Server, display the BD_ADDR*/
            /* of the connecting device.                                */
            BD_ADDRToStr(EventData->EventData.AudioDisconnectedEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.AudioDisconnectedEventData.ConnectionType, ConnectionStr);
            printf("hetHDSAudioDisconnected, BD_ADDR: %s, Type: %s\r\n", BoardStr, ConnectionStr);

            /* Flag that we would like the Audio Data Indication message*/
            /* to be displayed on the first reception of Audio Data.    */
            AudioDataCount = 0;
            break;
         case hetHDSAudioConnectionStatus:
            /* The Client received a Connect Confirmation, display      */
            /* relevant information.                                    */
            BD_ADDRToStr(EventData->EventData.AudioConnectionStatusEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.AudioConnectionStatusEventData.ConnectionType, ConnectionStr);
            printf("hetHDSAudioConnectionStatus, BD_ADDR: %s, Type: %s, Successfull: %s.\r\n", BoardStr, ConnectionStr, (EventData->EventData.AudioConnectionStatusEventData.Successful?"YES":"NO"));
            break;
         case hetHDSAudioData:
            /* To avoid flooding the user with Audio Data Messages, only*/
            /* print out a message every second.  The number below is   */
            /* loosely based on the fact that the Bluetooth             */
            /* Specification mentions 3ms per SCO Packet, which is what */
            /* most chips appear to use.  So we arrive at the 'magical' */
            /* number below.                                            */
            AudioDataCount--;
            if(!AudioDataCount)
            {
               /* An Audio Data Indication was received, display the    */
               /* relevant information.                                 */
               BD_ADDRToStr(EventData->EventData.AudioDataEventData.RemoteDeviceAddress, BoardStr);
               ConnectionTypeToStr(EventData->EventData.AudioDataEventData.ConnectionType, ConnectionStr);
               printf("\r\n");
               printf("hetHDSAudioData, BD_ADDR: %s, Type: %s, Length 0x%04X: %02X%02X%02X%02X.\r\n", BoardStr, ConnectionStr, EventData->EventData.AudioDataEventData.AudioDataLength, EventData->EventData.AudioDataEventData.AudioData[0], EventData->EventData.AudioDataEventData.AudioData[1], EventData->EventData.AudioDataEventData.AudioData[2], EventData->EventData.AudioDataEventData.AudioData[3]);
               printf("Headset>");

               fflush(stdout);

               /* Reset the Audio Data Count so we only print out       */
               /* received data a second later.                         */
               AudioDataCount = 333;
            }

            /* Simply loop the data back.                               */
            HDSM_Send_Audio_Data(EventData->EventData.AudioDataEventData.DataEventsHandlerID, EventData->EventData.AudioDataEventData.ConnectionType, EventData->EventData.AudioDataEventData.RemoteDeviceAddress, EventData->EventData.AudioDataEventData.AudioDataLength, EventData->EventData.AudioDataEventData.AudioData);
            break;
         case hetHDSSpeakerGainIndication:
            BD_ADDRToStr(EventData->EventData.SpeakerGainIndicationEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.SpeakerGainIndicationEventData.ConnectionType, ConnectionStr);

            /* A Ring Indication was received, display all relevant     */
            /* information.                                             */
            printf("hetHDSSpeakerGainIndication, BD_ADDR: %s, Type: %s, Speaker Gain: %u.\r\n", BoardStr, ConnectionStr, EventData->EventData.SpeakerGainIndicationEventData.SpeakerGain);
            break;
         case hetHDSMicrophoneGainIndication:
            BD_ADDRToStr(EventData->EventData.MicrophoneGainIndicationEventData.RemoteDeviceAddress, BoardStr);
            ConnectionTypeToStr(EventData->EventData.MicrophoneGainIndicationEventData.ConnectionType, ConnectionStr);

            /* A Ring Indication was received, display all relevant     */
            /* information.                                             */
            printf("hetHDSMicrophoneGainIndication, BD_ADDR: %s, Type: %s, Microphone Gain: %u.\r\n", BoardStr, ConnectionStr, EventData->EventData.MicrophoneGainIndicationEventData.MicrophoneGain);
            break;
         case hetHDSButtonPressedIndication:
            BD_ADDRToStr(EventData->EventData.ButtonPressIndicationEventData.RemoteDeviceAddress, BoardStr);

            printf("hetHDSButtonPressedIndication, BD_ADDR: %s\r\n", BoardStr);
            break;
         default:
            /* An unknown/unexpected HDS event was received.            */
            printf("\r\nUnknown HDSM Event Received: %d.\r\n", EventData->EventType);
            break;
      }
   }
   else
   {
      /* There was an error with one or more of the input parameters.   */
      printf("\r\nHDSM callback data: Event_Data = NULL.\r\n");
   }

   /* Output an Input Shell-type prompt.                                */
   if(EventData->EventType != hetHDSAudioData)
   {
      printf("Headset>");

      /* Make sure the output is displayed to the user.                 */
      fflush(stdout);
   }
}


/* The following function is the Device Manager Authentication Event */
/* Callback function that is Registered with the Device Manager.     */
/* This callback is responsible for processing all Device Manager    */
/* Authentication Request Events.                                    */
void BTPSAPI DEVM_Authentication_Callback(DEVM_Authentication_Information_t *
                                          AuthenticationRequestInformation, void *
                                          /*CallbackParameter*/)
{
   int                               Result;
   char                              Buffer[32];
   DEVM_Authentication_Information_t AuthenticationResponseInformation;

   if(AuthenticationRequestInformation)
   {
      printf("\r\n");

      BD_ADDRToStr(AuthenticationRequestInformation->BD_ADDR, Buffer);

      printf("Authentication Request received for %s.\r\n", Buffer);

      switch(AuthenticationRequestInformation->AuthenticationAction)
      {
         case DEVM_AUTHENTICATION_ACTION_PIN_CODE_REQUEST:
            printf("PIN Code Request.\r\n.");

            /* Note the current Remote BD_ADDR that is requesting the   */
            /* PIN Code.                                                */
            CurrentRemoteBD_ADDR = AuthenticationRequestInformation->BD_ADDR;

            /* Inform the user that they will need to respond with a PIN*/
            /* Code Response.                                           */
            printf("\r\nRespond with the command: PINCodeResponse\r\n");
            break;
         case DEVM_AUTHENTICATION_ACTION_USER_CONFIRMATION_REQUEST:
            printf("User Confirmation Request.\r\n");

            /* Note the current Remote BD_ADDR that is requesting the   */
            /* User Confirmation.                                       */
            CurrentRemoteBD_ADDR = AuthenticationRequestInformation->BD_ADDR;

            if(IOCapability != icDisplayYesNo)
            {
               /* Invoke Just works.                                    */

               printf("\r\nAuto Accepting: %lu\r\n", (unsigned long)AuthenticationRequestInformation->AuthenticationData.Passkey);

               BTPS_MemInitialize(&AuthenticationResponseInformation, 0, sizeof(AuthenticationResponseInformation));

               AuthenticationResponseInformation.BD_ADDR                         = AuthenticationRequestInformation->BD_ADDR;
               AuthenticationResponseInformation.AuthenticationAction            = DEVM_AUTHENTICATION_ACTION_USER_CONFIRMATION_RESPONSE;
               AuthenticationResponseInformation.AuthenticationDataLength        = sizeof(AuthenticationResponseInformation.AuthenticationData.Confirmation);

               AuthenticationResponseInformation.AuthenticationData.Confirmation = (Boolean_t)TRUE;

               if((Result = DEVM_AuthenticationResponse(AuthenticationCallbackID, &AuthenticationResponseInformation)) >= 0)
                  printf("DEVM_AuthenticationResponse() Success.\r\n");
               else
                  printf("DEVM_AuthenticationResponse() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));

               /* Flag that there is no longer a current Authentication */
               /* procedure in progress.                                */
               ASSIGN_BD_ADDR(CurrentRemoteBD_ADDR, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
            }
            else
            {
               printf("User Confirmation: %lu\r\n", (unsigned long)AuthenticationRequestInformation->AuthenticationData.Passkey);

               /* Inform the user that they will need to respond with a */
               /* PIN Code Response.                                    */
               printf("\r\nRespond with the command: UserConfirmationResponse\r\n");
            }
            break;
         case DEVM_AUTHENTICATION_ACTION_PASSKEY_REQUEST:
            printf("Passkey Request.\r\n");

            /* Note the current Remote BD_ADDR that is requesting the   */
            /* Passkey.                                                 */
            CurrentRemoteBD_ADDR = AuthenticationRequestInformation->BD_ADDR;

            /* Inform the user that they will need to respond with a    */
            /* Passkey Response.                                        */
            printf("\r\nRespond with the command: PassKeyResponse\r\n");
            break;
         case DEVM_AUTHENTICATION_ACTION_PASSKEY_INDICATION:
            printf("PassKey Indication.\r\n");

            printf("PassKey: %lu\r\n", (unsigned long)AuthenticationRequestInformation->AuthenticationData.Passkey);
            break;
         case DEVM_AUTHENTICATION_ACTION_KEYPRESS_INDICATION:
            printf("Keypress Indication.\r\n");

            printf("Keypress: %d\r\n", (int)AuthenticationRequestInformation->AuthenticationData.Keypress);
            break;
         case DEVM_AUTHENTICATION_ACTION_OUT_OF_BAND_DATA_REQUEST:
            printf("Out of Band Data Request.\r\n");

            /* This application does not support OOB data so respond    */
            /* with a data length of Zero to force a negative reply.    */
            BTPS_MemInitialize(&AuthenticationResponseInformation, 0, sizeof(AuthenticationResponseInformation));

            AuthenticationResponseInformation.BD_ADDR                  = AuthenticationRequestInformation->BD_ADDR;
            AuthenticationResponseInformation.AuthenticationAction     = DEVM_AUTHENTICATION_ACTION_OUT_OF_BAND_DATA_RESPONSE;
            AuthenticationResponseInformation.AuthenticationDataLength = 0;

            if((Result = DEVM_AuthenticationResponse(AuthenticationCallbackID, &AuthenticationResponseInformation)) >= 0)
               printf("DEVM_AuthenticationResponse() Success.\r\n");
            else
               printf("DEVM_AuthenticationResponse() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));
            break;
         case DEVM_AUTHENTICATION_ACTION_IO_CAPABILITIES_REQUEST:
            printf("I/O Capability Request.\r\n");

            /* Respond with the currently configured I/O Capabilities.  */
            BTPS_MemInitialize(&AuthenticationResponseInformation, 0, sizeof(AuthenticationResponseInformation));

            AuthenticationResponseInformation.BD_ADDR                                                    = AuthenticationRequestInformation->BD_ADDR;
            AuthenticationResponseInformation.AuthenticationAction                                       = DEVM_AUTHENTICATION_ACTION_IO_CAPABILITIES_RESPONSE;
            AuthenticationResponseInformation.AuthenticationDataLength                                   = sizeof(AuthenticationResponseInformation.AuthenticationData.IOCapabilities);

            AuthenticationResponseInformation.AuthenticationData.IOCapabilities.IO_Capability            = (GAP_IO_Capability_t)IOCapability;
            AuthenticationResponseInformation.AuthenticationData.IOCapabilities.MITM_Protection_Required = MITMProtection;
            AuthenticationResponseInformation.AuthenticationData.IOCapabilities.OOB_Data_Present         = OOBSupport;

            if((Result = DEVM_AuthenticationResponse(AuthenticationCallbackID, &AuthenticationResponseInformation)) >= 0)
               printf("DEVM_AuthenticationResponse() Success.\r\n");
            else
               printf("DEVM_AuthenticationResponse() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));
            break;
         case DEVM_AUTHENTICATION_ACTION_IO_CAPABILITIES_RESPONSE:
            printf("I/O Capability Response.\r\n");

            /* Inform the user of the Remote I/O Capablities.           */
            printf("Remote I/O Capabilities: %s, MITM Protection: %s.\r\n", IOCapabilitiesStrings[AuthenticationRequestInformation->AuthenticationData.IOCapabilities.IO_Capability], AuthenticationRequestInformation->AuthenticationData.IOCapabilities.MITM_Protection_Required?"TRUE":"FALSE");
            break;
         case DEVM_AUTHENTICATION_ACTION_AUTHENTICATION_STATUS_RESULT:
            printf("Authentication Status.\r\n");

            printf("Status: %d\r\n", AuthenticationRequestInformation->AuthenticationData.AuthenticationStatus);
            break;
         case DEVM_AUTHENTICATION_ACTION_AUTHENTICATION_START:
            printf("Authentication Start.\r\n");
            break;
         case DEVM_AUTHENTICATION_ACTION_AUTHENTICATION_END:
            printf("Authentication End.\r\n");
            break;
         default:
            printf("Unknown Device Manager Authentication Event Received: 0x%08X, Length: 0x%08X.\r\n", (unsigned int)AuthenticationRequestInformation->AuthenticationAction, AuthenticationRequestInformation->AuthenticationDataLength);
            break;
      }
   }
   else
      printf("\r\nDEVM Authentication Request Data is NULL.\r\n");

   printf("Headset>");

   /* Make sure the output is displayed to the user.                    */
   fflush(stdout);
}


/* The following function is responsible for the specified string    */
/* into data of type BD_ADDR.  The first parameter of this function  */
/* is the BD_ADDR string to be converted to a BD_ADDR.  The second   */
/* parameter of this function is a pointer to the BD_ADDR in which   */
/* the converted BD_ADDR String is to be stored.                     */
void StrToBD_ADDR(char *BoardStr, BD_ADDR_t *Board_Address)
{
   unsigned int Address[sizeof(BD_ADDR_t)];

   if((BoardStr) && (strlen(BoardStr) == sizeof(BD_ADDR_t)*2) && (Board_Address))
   {
      sscanf(BoardStr, "%02X%02X%02X%02X%02X%02X", &(Address[5]), &(Address[4]), &(Address[3]), &(Address[2]), &(Address[1]), &(Address[0]));

      Board_Address->BD_ADDR5 = (Byte_t)Address[5];
      Board_Address->BD_ADDR4 = (Byte_t)Address[4];
      Board_Address->BD_ADDR3 = (Byte_t)Address[3];
      Board_Address->BD_ADDR2 = (Byte_t)Address[2];
      Board_Address->BD_ADDR1 = (Byte_t)Address[1];
      Board_Address->BD_ADDR0 = (Byte_t)Address[0];
   }
   else
   {
      if(Board_Address)
         BTPS_MemInitialize(Board_Address, 0, sizeof(BD_ADDR_t));
   }
}


/* The following function is responsible for displaying the contents */
/* of Parsed Remote Device Services Data to the display.             */
void DisplayParsedServiceData(DEVM_Parsed_SDP_Data_t *ParsedSDPData)
{
   unsigned int Index;

   /* First, check to see if Service Records were returned.             */
   if((ParsedSDPData) && (ParsedSDPData->NumberServiceRecords))
   {
      /* Loop through all returned SDP Service Records.                 */
      for(Index=0; Index<ParsedSDPData->NumberServiceRecords; Index++)
      {
         /* First display the number of SDP Service Records we are      */
         /* currently processing.                                       */
         printf("Service Record: %u:\r\n", (Index + 1));

         /* Call Display SDPAttributeResponse for all SDP Service       */
         /* Records received.                                           */
         DisplaySDPAttributeResponse(&(ParsedSDPData->SDPServiceAttributeResponseData[Index]), 1);
      }
   }
   else
      printf("No SDP Service Records Found.\r\n");
}


/* The following function is used to get the Headset Service         */
/* information from a specified remote device.                       */
int GetHeadsetServiceInformation(BD_ADDR_t BD_ADDR, Boolean_t ForceUpdate, unsigned int *PortNumber)
{
   int                     ret_val;
   Boolean_t               ServiceLocated;
   unsigned int            TotalServiceSize;
   unsigned char          *ServiceData;
   HDS_Profile_Info_t     HDSProfileInfo;
   DEVM_Parsed_SDP_Data_t  ParsedSDPData;

   /* Determine the total number of bytes that are stored for this      */
   if(!(ret_val = DEVM_QueryRemoteDeviceServices(BD_ADDR, ForceUpdate, 0, NULL, &TotalServiceSize)))
   {
      /* Determine if we are being asked to display the Service         */
      /* Information.                                                   */
      if(!ForceUpdate)
      {
         /* Determine if there is any service data for this device.     */
         if(TotalServiceSize)
         {
            /* Allocate memory to hold all of the service data.         */
            if((ServiceData = (unsigned char *)BTPS_AllocateMemory(TotalServiceSize)) != NULL)
            {
               /* Query the remote device services.                     */
               if((ret_val = DEVM_QueryRemoteDeviceServices(BD_ADDR, ForceUpdate, TotalServiceSize, ServiceData, &TotalServiceSize)) >= 0)
               {
                  /* Convert the raw SDP stream to a parsed SDP record. */
                  ret_val = DEVM_ConvertRawSDPStreamToParsedSDPData(ret_val, ServiceData, &ParsedSDPData);
                  if(!ret_val)
                  {
                     /* Determine if a Headset Audio gateway service is */
                     /* located on the remote device.                   */
                     ServiceLocated = ParseHeadsetData(&ParsedSDPData, &HDSProfileInfo);
                     if(ServiceLocated)
                     {
                        if((HDSProfileInfo.ServiceNameLength) && (HDSProfileInfo.ServiceNameLength < TotalServiceSize))
                        {
                           BTPS_MemCopy(ServiceData, HDSProfileInfo.ServiceName, HDSProfileInfo.ServiceNameLength);
                           ServiceData[HDSProfileInfo.ServiceNameLength] = '\0';
                           printf("Service Name: %s.\r\n", ServiceData);
                        }

                        if((HDSProfileInfo.ServiceProviderLength) && (HDSProfileInfo.ServiceProviderLength < TotalServiceSize))
                        {
                           BTPS_MemCopy(ServiceData, HDSProfileInfo.ServiceProvider, HDSProfileInfo.ServiceProviderLength);
                           ServiceData[HDSProfileInfo.ServiceProviderLength] = '\0';
                           printf("Service Provider: %s.\r\n", ServiceData);
                        }

                        if((HDSProfileInfo.ServiceDescLength) && (HDSProfileInfo.ServiceDescLength < TotalServiceSize))
                        {
                           BTPS_MemCopy(ServiceData, HDSProfileInfo.ServiceDesc, HDSProfileInfo.ServiceDescLength);
                           ServiceData[HDSProfileInfo.ServiceDescLength] = '\0';
                           printf("Service Description: %s.\r\n", ServiceData);
                        }

                        printf("RFCOMM Port Number: 0x%02X.\r\n", HDSProfileInfo.HDSInfo.ServerChannel);
                        printf("HDS Profile Version: 0x%04X.\r\n", HDSProfileInfo.HDSInfo.ProfileVersion);

                        /* Return the port number if requested.         */
                        if(PortNumber)
                           *PortNumber = (unsigned int)HDSProfileInfo.HDSInfo.ServerChannel;
                     }
                     else
                        printf("Headset Audio Gateway Server not located on remote device.\r\n");

                     /* All finished with the parsed data, so free it.  */
                     DEVM_FreeParsedSDPData(&ParsedSDPData);
                  }
                  else
                  {
                     printf("DEVM_ConvertRawSDPStreamToParsedSDPData() Failure: %d, %s.\r\n", ret_val, ERR_ConvertErrorCodeToString(ret_val));

                     ret_val = FUNCTION_ERROR;
                  }
               }
               else
               {
                  /* Error attempting to query Services, inform the user*/
                  /* and flag an error.                                 */
                  printf("DEVM_QueryRemoteDeviceServices() Failure: %d, %s.\r\n", ret_val, ERR_ConvertErrorCodeToString(ret_val));

                  ret_val = FUNCTION_ERROR;
               }

               /* Free the previously allocated memory.                 */
               BTPS_FreeMemory(ServiceData);
            }
            else
            {
               printf("Failed to allocate memory for the service data for %u bytes.\r\n", TotalServiceSize);

               ret_val = FUNCTION_ERROR;
            }
         }
         else
         {
            printf("Total Service Size is 0.\r\n");

            ret_val = FUNCTION_ERROR;
         }
      }
      else
      {
         printf("Querying services for remote device.\r\n");

         ret_val = 0;
      }
   }
   else
   {
      /* Error attempting to query Services, inform the user and flag an*/
      /* error.                                                         */
      printf("DEVM_QueryRemoteDeviceServices() Failure: %d, %s.\r\n", ret_val, ERR_ConvertErrorCodeToString(ret_val));

      ret_val = FUNCTION_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for Cleaning up/Shutting    */
/* down the Bluetopia Platform Manager Framework.  This function     */
/* returns zero if successful and a negative value if an error       */
/* occurred.                                                         */
int Cleanup(ParameterList_t */*TempParam*/)
{
   int ret_val;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* If there was an Event Callback Registered, then we need to     */
      /* un-register it.                                                */
      if(DEVMCallbackID)
         DEVM_UnRegisterEventCallback(DEVMCallbackID);

      if(AuthenticationCallbackID)
         DEVM_UnRegisterAuthentication(AuthenticationCallbackID);

      if(HDSventCallbackID)
         HDSM_Un_Register_Event_Callback(HDSventCallbackID);

      if(HDSDataCallbackID)
         HDSM_Un_Register_Data_Event_Callback(HDSDataCallbackID);

      /* Nothing to do other than to clean up the Bluetopia Platform    */
      /* Manager Service and flag that it is no longer initialized.     */
      BTPM_Cleanup();

      Initialized              = FALSE;
      DEVMCallbackID           = 0;
      AuthenticationCallbackID = 0;
      HDSventCallbackID       = 0;
      HDSDataCallbackID        = 0;
      ASSIGN_BD_ADDR(CurrentRemoteBD_ADDR, 0, 0, 0, 0, 0, 0);

      ret_val                  = 0;
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is a utility function that exists to       */
/* display either the entire Local Device Property information (first*/
/* parameter is zero) or portions that have changed.                 */
void DisplayLocalDeviceProperties(unsigned long UpdateMask, DEVM_Local_Device_Properties_t *LocalDeviceProperties)
{
   char Buffer[64];

   if(LocalDeviceProperties)
   {
      /* First, display any information that is not part of any update  */
      /* mask.                                                          */
      if(!UpdateMask)
      {
         BD_ADDRToStr(LocalDeviceProperties->BD_ADDR, Buffer);

         printf("BD_ADDR:      %s\r\n", Buffer);
         printf("HCI Ver:      0x%04X\r\n", (Word_t)LocalDeviceProperties->HCIVersion);
         printf("HCI Rev:      0x%04X\r\n", (Word_t)LocalDeviceProperties->HCIRevision);
         printf("LMP Ver:      0x%04X\r\n", (Word_t)LocalDeviceProperties->LMPVersion);
         printf("LMP Sub Ver:  0x%04X\r\n", (Word_t)LocalDeviceProperties->LMPSubVersion);
         printf("Device Man:   0x%04X (%s)\r\n", (Word_t)LocalDeviceProperties->DeviceManufacturer, DEVM_ConvertManufacturerNameToString(LocalDeviceProperties->DeviceManufacturer));
         printf("Device Flags: 0x%08lX\r\n", LocalDeviceProperties->LocalDeviceFlags);
      }

      if((!UpdateMask) || (UpdateMask & DEVM_LOCAL_DEVICE_PROPERTIES_CHANGED_CLASS_OF_DEVICE))
         printf("COD:          0x%02X%02X%02X\r\n", LocalDeviceProperties->ClassOfDevice.Class_of_Device0, LocalDeviceProperties->ClassOfDevice.Class_of_Device1, LocalDeviceProperties->ClassOfDevice.Class_of_Device2);

      if((!UpdateMask) || (UpdateMask & DEVM_LOCAL_DEVICE_PROPERTIES_CHANGED_DEVICE_NAME))
         printf("Device Name:  %s\r\n", (LocalDeviceProperties->DeviceNameLength)?LocalDeviceProperties->DeviceName:"");

      if((!UpdateMask) || (UpdateMask & DEVM_LOCAL_DEVICE_PROPERTIES_CHANGED_DISCOVERABLE_MODE))
         printf("Disc. Mode:   %s, 0x%08X\r\n", LocalDeviceProperties->DiscoverableMode?"TRUE ":"FALSE", LocalDeviceProperties->DiscoverableModeTimeout);

      if((!UpdateMask) || (UpdateMask & DEVM_LOCAL_DEVICE_PROPERTIES_CHANGED_CONNECTABLE_MODE))
         printf("Conn. Mode:   %s, 0x%08X\r\n", LocalDeviceProperties->ConnectableMode?"TRUE ":"FALSE", LocalDeviceProperties->ConnectableModeTimeout);

      if((!UpdateMask) || (UpdateMask & DEVM_LOCAL_DEVICE_PROPERTIES_CHANGED_PAIRABLE_MODE))
         printf("Pair. Mode:   %s, 0x%08X\r\n", LocalDeviceProperties->PairableMode?"TRUE ":"FALSE", LocalDeviceProperties->PairableModeTimeout);
   }
}


/* The following function is a utility function that exists to       */
/* display either the entire Remote Device Property information      */
/* (first parameter is zero) or portions that have changed.          */
void DisplayRemoteDeviceProperties(unsigned long UpdateMask, DEVM_Remote_Device_Properties_t *RemoteDeviceProperties)
{
   char Buffer[64];

   if(RemoteDeviceProperties)
   {
      /* First, display any information that is not part of any update  */
      /* mask.                                                          */
      BD_ADDRToStr(RemoteDeviceProperties->BD_ADDR, Buffer);

      printf("BD_ADDR:       %s\r\n", Buffer);

      if((!UpdateMask) || (UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_CLASS_OF_DEVICE))
         printf("COD:           0x%02X%02X%02X\r\n", RemoteDeviceProperties->ClassOfDevice.Class_of_Device0, RemoteDeviceProperties->ClassOfDevice.Class_of_Device1, RemoteDeviceProperties->ClassOfDevice.Class_of_Device2);

      if((!UpdateMask) || (UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_DEVICE_NAME))
         printf("Device Name:   %s\r\n", (RemoteDeviceProperties->DeviceNameLength)?RemoteDeviceProperties->DeviceName:"");

      if((!UpdateMask) || (UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_DEVICE_FLAGS))
         printf("Device Flags:  0x%08lX\r\n", RemoteDeviceProperties->RemoteDeviceFlags);

      if((!UpdateMask) || (UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_RSSI))
         printf("RSSI:          %d\r\n", RemoteDeviceProperties->RSSI);

      if((!UpdateMask) && (RemoteDeviceProperties->RemoteDeviceFlags & DEVM_REMOTE_DEVICE_FLAGS_DEVICE_CURRENTLY_CONNECTED))
         printf("Trans. Power:  %d\r\n", RemoteDeviceProperties->TransmitPower);

      if((!UpdateMask) || ((UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_APPLICATION_DATA) && (RemoteDeviceProperties->RemoteDeviceFlags & DEVM_REMOTE_DEVICE_FLAGS_DEVICE_APPLICATION_DATA_VALID)))
      {
         printf("Friendly Name: %s\r\n", (RemoteDeviceProperties->ApplicationData.FriendlyNameLength)?RemoteDeviceProperties->ApplicationData.FriendlyName:"");

         printf("App. Info:   : %08lX\r\n", RemoteDeviceProperties->ApplicationData.ApplicationInfo);
      }

      if((!UpdateMask) || (UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_PAIRING_STATE))
         printf("Paired State : %s\r\n", (RemoteDeviceProperties->RemoteDeviceFlags & DEVM_REMOTE_DEVICE_FLAGS_DEVICE_CURRENTLY_PAIRED)?"TRUE":"FALSE");

      if((!UpdateMask) || (UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_CONNECTION_STATE))
         printf("Connect State: %s\r\n", (RemoteDeviceProperties->RemoteDeviceFlags & DEVM_REMOTE_DEVICE_FLAGS_DEVICE_CURRENTLY_CONNECTED)?"TRUE":"FALSE");

      if((!UpdateMask) || (UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_ENCRYPTION_STATE))
         printf("Encrypt State: %s\r\n", (RemoteDeviceProperties->RemoteDeviceFlags & DEVM_REMOTE_DEVICE_FLAGS_DEVICE_LINK_CURRENTLY_ENCRYPTED)?"TRUE":"FALSE");

      if((!UpdateMask) || (UpdateMask & DEVM_REMOTE_DEVICE_PROPERTIES_CHANGED_SERVICES_STATE))
         printf("Serv. Known  : %s\r\n", (RemoteDeviceProperties->RemoteDeviceFlags & DEVM_REMOTE_DEVICE_FLAGS_DEVICE_SERVICES_KNOWN)?"TRUE":"FALSE");
   }
}


/* The following function is responsible for converting data of type */
/* BD_ADDR to a string.  The first parameter of this function is the */
/* BD_ADDR to be converted to a string.  The second parameter of this*/
/* function is a pointer to the string in which the converted BD_ADDR*/
/* is to be stored.                                                  */
void BD_ADDRToStr(BD_ADDR_t Board_Address, char *BoardStr)
{
   sprintf(BoardStr, "%02X%02X%02X%02X%02X%02X", Board_Address.BD_ADDR5,
                                                 Board_Address.BD_ADDR4,
                                                 Board_Address.BD_ADDR3,
                                                 Board_Address.BD_ADDR2,
                                                 Board_Address.BD_ADDR1,
                                                 Board_Address.BD_ADDR0);
}


/* The following function is responsible for converting a HDSM       */
/* Connection Type to a string.  The first parameter to this function*/
/* is the ConnectionType to be converted to a string.  The second    */
/* parameter of this function is a pointer to a string in which the  */
/* converted Connection Type is to be stored.                        */
void ConnectionTypeToStr(HDSM_Connection_Type_t ConnectionType, ConnectionTypeStr_t ConnectionTypeStr)
{
   if(ConnectionType == sctHeadset)
      sprintf(ConnectionTypeStr, "sctHeadset");
   else
   {
      if(ConnectionType == sctAudioGateway)
         sprintf(ConnectionTypeStr, "sctAudioGateway");
      else
         sprintf(ConnectionTypeStr, "Unknown");
   }
}


/* The following function is responsible for Displaying the contents */
/* of an SDP Service Attribute Response to the display.              */
void DisplaySDPAttributeResponse(SDP_Service_Attribute_Response_Data_t *SDPServiceAttributeResponse,
                                 unsigned int InitLevel)
{
   int Index;

   /* First, check to make sure that there were Attributes returned.    */
   if(SDPServiceAttributeResponse->Number_Attribute_Values)
   {
      /* Loop through all returned SDP Attribute Values.                */
      for(Index = 0; Index < SDPServiceAttributeResponse->Number_Attribute_Values; Index++)
      {
         /* First Print the Attribute ID that was returned.             */
         printf("%*s Attribute ID 0x%04X\r\n", (InitLevel*INDENT_LENGTH), "",
                SDPServiceAttributeResponse->SDP_Service_Attribute_Value_Data[Index].Attribute_ID);

         /* Now Print out all of the SDP Data Elements that were        */
         /* returned that are associated with the SDP Attribute.        */
         DisplayDataElement(SDPServiceAttributeResponse->SDP_Service_Attribute_Value_Data[Index].SDP_Data_Element,
                            (InitLevel + 1));
      }
   }
   else
      printf("No SDP Attributes Found.\r\n");
}


/* The following function is used to parse a SDP data stream into a  */
/* Headset Audio profile information structure structure.  This      */
/* function returns TRUE if a Headset record was parsed from the SDP */
/* data stream or FALSE otherwise                                    */
Boolean_t ParseHeadsetData(DEVM_Parsed_SDP_Data_t *ParsedSDPData,
                           HDS_Profile_Info_t *ProfileInfo)
{
   Boolean_t    ret_val = FALSE;
   unsigned int Index;

   /* Verify that the input parameters are semi-valid.                  */
   if((ParsedSDPData) && (ProfileInfo))
   {
      /* Loop through all returned SDP Service Records.                 */
      for(Index=0;(Index<ParsedSDPData->NumberServiceRecords)&&(!ret_val);Index++)
      {
         /* Call Display SDPAttributeResponse for all SDP Service       */
         /* Records received.                                           */
         ret_val = ParseHeadsetSDPAttributeResponse(&(ParsedSDPData->SDPServiceAttributeResponseData[Index]), ProfileInfo);
      }
   }

   return(ret_val);
}


/* The following function is responsible for actually displaying an  */
/* individual SDP Data Element to the Display.  The Level Parameter  */
/* is used in conjunction with the defined INDENT_LENGTH constant to */
/* make readability easier when displaying Data Element Sequences    */
/* and Data Element Alternatives.  This function will recursively    */
/* call itself to display the contents of Data Element Sequences and */
/* Data Element Alternatives when it finds these Data Types (and     */
/* increments the Indent Level accordingly).                         */
void DisplayDataElement(SDP_Data_Element_t *SDPDataElement, unsigned int Level)
{
   unsigned int Index;
   char         Buffer[256];

   switch(SDPDataElement->SDP_Data_Element_Type)
   {
      case deNIL:
         /* Display the NIL Type.                                       */
         printf("%*s Type: NIL\r\n", (Level*INDENT_LENGTH), "");
         break;
      case deNULL:
         /* Display the NULL Type.                                      */
         printf("%*s Type: NULL\r\n", (Level*INDENT_LENGTH), "");
         break;
      case deUnsignedInteger1Byte:
         /* Display the Unsigned Integer (1 Byte) Type.                 */
         printf("%*s Type: Unsigned Int = 0x%02X\r\n", (Level*INDENT_LENGTH), "", SDPDataElement->SDP_Data_Element.UnsignedInteger1Byte);
         break;
      case deUnsignedInteger2Bytes:
         /* Display the Unsigned Integer (2 Bytes) Type.                */
         printf("%*s Type: Unsigned Int = 0x%04X\r\n", (Level*INDENT_LENGTH), "", SDPDataElement->SDP_Data_Element.UnsignedInteger2Bytes);
         break;
      case deUnsignedInteger4Bytes:
         /* Display the Unsigned Integer (4 Bytes) Type.                */
         printf("%*s Type: Unsigned Int = 0x%08X\r\n", (Level*INDENT_LENGTH), "", (unsigned int)SDPDataElement->SDP_Data_Element.UnsignedInteger4Bytes);
         break;
      case deUnsignedInteger8Bytes:
         /* Display the Unsigned Integer (8 Bytes) Type.                */
         printf("%*s Type: Unsigned Int = 0x%02X%02X%02X%02X%02X%02X%02X%02X\r\n", (Level*INDENT_LENGTH), "",
                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger8Bytes[7],
                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger8Bytes[6],
                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger8Bytes[5],
                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger8Bytes[4],
                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger8Bytes[3],
                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger8Bytes[2],
                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger8Bytes[1],
                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger8Bytes[0]);
         break;
      case deUnsignedInteger16Bytes:
         /* Display the Unsigned Integer (16 Bytes) Type.               */
         printf("%*s Type: Unsigned Int = 0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\r\n", (Level*INDENT_LENGTH), "",
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[15],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[14],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[13],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[12],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[11],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[10],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[9],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[8],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[7],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[6],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[5],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[4],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[3],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[2],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[1],
                                                                                                                 SDPDataElement->SDP_Data_Element.UnsignedInteger16Bytes[0]);
         break;
      case deSignedInteger1Byte:
         /* Display the Signed Integer (1 Byte) Type.                   */
         printf("%*s Type: Signed Int = 0x%02X\r\n", (Level*INDENT_LENGTH), "", SDPDataElement->SDP_Data_Element.SignedInteger1Byte);
         break;
      case deSignedInteger2Bytes:
         /* Display the Signed Integer (2 Bytes) Type.                  */
         printf("%*s Type: Signed Int = 0x%04X\r\n", (Level*INDENT_LENGTH), "", SDPDataElement->SDP_Data_Element.SignedInteger2Bytes);
         break;
      case deSignedInteger4Bytes:
         /* Display the Signed Integer (4 Bytes) Type.                  */
         printf("%*s Type: Signed Int = 0x%08X\r\n", (Level*INDENT_LENGTH), "", (unsigned int)SDPDataElement->SDP_Data_Element.SignedInteger4Bytes);
         break;
      case deSignedInteger8Bytes:
         /* Display the Signed Integer (8 Bytes) Type.                  */
         printf("%*s Type: Signed Int = 0x%02X%02X%02X%02X%02X%02X%02X%02X\r\n", (Level*INDENT_LENGTH), "",
                                                                               SDPDataElement->SDP_Data_Element.SignedInteger8Bytes[7],
                                                                               SDPDataElement->SDP_Data_Element.SignedInteger8Bytes[6],
                                                                               SDPDataElement->SDP_Data_Element.SignedInteger8Bytes[5],
                                                                               SDPDataElement->SDP_Data_Element.SignedInteger8Bytes[4],
                                                                               SDPDataElement->SDP_Data_Element.SignedInteger8Bytes[3],
                                                                               SDPDataElement->SDP_Data_Element.SignedInteger8Bytes[2],
                                                                               SDPDataElement->SDP_Data_Element.SignedInteger8Bytes[1],
                                                                               SDPDataElement->SDP_Data_Element.SignedInteger8Bytes[0]);
         break;
      case deSignedInteger16Bytes:
         /* Display the Signed Integer (16 Bytes) Type.                 */
         printf("%*s Type: Signed Int = 0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\r\n", (Level*INDENT_LENGTH), "",
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[15],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[14],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[13],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[12],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[11],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[10],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[9],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[8],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[7],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[6],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[5],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[4],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[3],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[2],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[1],
                                                                                                               SDPDataElement->SDP_Data_Element.SignedInteger16Bytes[0]);
         break;
      case deTextString:
         /* First retrieve the Length of the Text String so that we can */
         /* copy the Data into our Buffer.                              */
         Index = (SDPDataElement->SDP_Data_Element_Length < sizeof(Buffer))?SDPDataElement->SDP_Data_Element_Length:(sizeof(Buffer)-1);

         /* Copy the Text String into the Buffer and then NULL terminate*/
         /* it.                                                         */
         memcpy(Buffer, SDPDataElement->SDP_Data_Element.TextString, Index);
         Buffer[Index] = '\0';

         printf("%*s Type: Text String = %s\r\n", (Level*INDENT_LENGTH), "", Buffer);
         break;
      case deBoolean:
         printf("%*s Type: Boolean = %s\r\n", (Level*INDENT_LENGTH), "", (SDPDataElement->SDP_Data_Element.Boolean)?"TRUE":"FALSE");
         break;
      case deURL:
         /* First retrieve the Length of the URL String so that we can  */
         /* copy the Data into our Buffer.                              */
         Index = (SDPDataElement->SDP_Data_Element_Length < sizeof(Buffer))?SDPDataElement->SDP_Data_Element_Length:(sizeof(Buffer)-1);

         /* Copy the URL String into the Buffer and then NULL terminate */
         /* it.                                                         */
         memcpy(Buffer, SDPDataElement->SDP_Data_Element.URL, Index);
         Buffer[Index] = '\0';

         printf("%*s Type: URL = %s\r\n", (Level*INDENT_LENGTH), "", Buffer);
         break;
      case deUUID_16:
         printf("%*s Type: UUID_16 = 0x%02X%02X\r\n", (Level*INDENT_LENGTH), "",
                                                    SDPDataElement->SDP_Data_Element.UUID_16.UUID_Byte0,
                                                    SDPDataElement->SDP_Data_Element.UUID_16.UUID_Byte1);
         break;
      case deUUID_32:
         printf("%*s Type: UUID_32 = 0x%02X%02X%02X%02X\r\n", (Level*INDENT_LENGTH), "",
                                                            SDPDataElement->SDP_Data_Element.UUID_32.UUID_Byte0,
                                                            SDPDataElement->SDP_Data_Element.UUID_32.UUID_Byte1,
                                                            SDPDataElement->SDP_Data_Element.UUID_32.UUID_Byte2,
                                                            SDPDataElement->SDP_Data_Element.UUID_32.UUID_Byte3);
         break;
      case deUUID_128:
         printf("%*s Type: UUID_128 = 0x%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\r\n", (Level*INDENT_LENGTH), "",
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte0,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte1,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte2,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte3,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte4,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte5,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte6,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte7,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte8,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte9,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte10,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte11,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte12,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte13,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte14,
                                                                                                             SDPDataElement->SDP_Data_Element.UUID_128.UUID_Byte15);
         break;
      case deSequence:
         /* Display that this is a SDP Data Element Sequence.           */
         printf("%*s Type: Data Element Sequence\r\n", (Level*INDENT_LENGTH), "");

         /* Loop through each of the SDP Data Elements in the SDP Data  */
         /* Element Sequence.                                           */
         for(Index = 0; Index < SDPDataElement->SDP_Data_Element_Length; Index++)
         {
            /* Call this function again for each of the SDP Data        */
            /* Elements in this SDP Data Element Sequence.              */
            DisplayDataElement(&(SDPDataElement->SDP_Data_Element.SDP_Data_Element_Sequence[Index]), (Level + 1));
         }
         break;
      case deAlternative:
         /* Display that this is a SDP Data Element Alternative.        */
         printf("%*s Type: Data Element Alternative\r\n", (Level*INDENT_LENGTH), "");

         /* Loop through each of the SDP Data Elements in the SDP Data  */
         /* Element Alternative.                                        */
         for(Index = 0; Index < SDPDataElement->SDP_Data_Element_Length; Index++)
         {
            /* Call this function again for each of the SDP Data        */
            /* Elements in this SDP Data Element Alternative.           */
            DisplayDataElement(&(SDPDataElement->SDP_Data_Element.SDP_Data_Element_Alternative[Index]), (Level + 1));
         }
         break;
      default:
         printf("%*s Unknown SDP Data Element Type\r\n", (Level*INDENT_LENGTH), "");
         break;
   }
}


/* The following function is used to process an SDP Service Record   */
/* and determine if it is a Headset record and process the           */
/* appropriate fields if it is.  This function returns TRUE if       */
/* successful or FALSE otherwise.                                    */
Boolean_t ParseHeadsetSDPAttributeResponse(SDP_Service_Attribute_Response_Data_t *SDPServiceAttributeResponse, HDS_Profile_Info_t *ProfileInfo)
{
   unsigned int        Index;
   unsigned int        Index1;
   Boolean_t           ret_val = FALSE;
   SDP_Record_t        SDPRecordInfo;
   SDP_Data_Element_t *DataElement1;
   SDP_Data_Element_t *DataElement2;
   SDP_Data_Element_t *DataElement3;

   /* Verify that the input parameters are semi-valid.                  */
   if((SDPServiceAttributeResponse) && (ProfileInfo))
   {
      /* First, check to make sure that there were Attributes returned. */
      if(SDPServiceAttributeResponse->Number_Attribute_Values)
      {
         /* Clear the SDP Record Info structure.                        */
         BTPS_MemInitialize(&SDPRecordInfo, 0, sizeof(SDP_Record_t));

         /* Loop through all returned SDP Attribute Values.             */
         for(Index1 = 0; Index1 < SDPServiceAttributeResponse->Number_Attribute_Values; Index1++)
         {
            /* Get a pointer to the first data element for the current  */
            /* attribute.                                               */
            DataElement1 = SDPServiceAttributeResponse->SDP_Service_Attribute_Value_Data[Index1].SDP_Data_Element;
            switch(SDPServiceAttributeResponse->SDP_Service_Attribute_Value_Data[Index1].Attribute_ID)
            {
               case SDP_ATTRIBUTE_ID_SERVICE_CLASS_ID_LIST:
                  /* Verify that the data element is what we expected.  */
                  if(DataElement1->SDP_Data_Element_Type == deSequence)
                  {
                     /* Get a pointer to the first data element and skip*/
                     /* over the L2CAP UUID element.                    */
                     DataElement2 = DataElement1->SDP_Data_Element.SDP_Data_Element_Sequence;
                     Index        = DataElement1->SDP_Data_Element_Length;
                     while((Index--) && (SDPRecordInfo.NumServiceClass < MAX_SDP_RECORD_ARRAY_LIST))
                     {
                        /* Extract a UUID_16_t from the Data Element;   */
                        SDPRecordInfo.ServiceClass[SDPRecordInfo.NumServiceClass] = ExtractUUID_ID(DataElement2);

                        /* Increment the number of Service Classes that */
                        /* have been processed.                         */
                        SDPRecordInfo.NumServiceClass++;

                        /* Advance to the next Data Element.            */
                        DataElement2++;
                     }
                  }
                  break;
               case SDP_ATTRIBUTE_ID_PROTOCOL_DESCRIPTOR_LIST:
                  /* Verify that the data element is what we expected.  */
                  if(DataElement1->SDP_Data_Element_Type == deSequence)
                  {
                     /* The Protocol Descriptor List is a sequence of   */
                     /* Data Element sequences, where the 1st sequence  */
                     /* identifies L2CAP and the registered L2CAP PSM.  */
                     DataElement2 = DataElement1->SDP_Data_Element.SDP_Data_Element_Sequence;
                     Index        = DataElement1->SDP_Data_Element_Length;

                     while((Index--) && (SDPRecordInfo.NumProtocols < MAX_SDP_RECORD_ARRAY_LIST))
                     {
                        /* Verify that this element is a Data Element   */
                        /* Sequence.                                    */
                        if(DataElement2->SDP_Data_Element_Type == deSequence)
                        {
                           /* Get the start of the next sequence.       */
                           DataElement3 = DataElement2->SDP_Data_Element.SDP_Data_Element_Sequence;

                           /* Extract a UUID_16_t from the Data Element */
                           SDPRecordInfo.Protocol[SDPRecordInfo.NumProtocols].UUID_ID = ExtractUUID_ID(DataElement3);

                           /* If the Length was greater than 1, then    */
                           /* there is an additional Byte or Word that  */
                           /* follows.                                  */
                           if(DataElement2->SDP_Data_Element_Length > 1)
                           {
                              /* Advance to the next element and get the*/
                              /* value that follows.                    */
                              DataElement3++;
                              if(SDPRecordInfo.NumProtocols < MAX_SDP_RECORD_ARRAY_LIST)
                              {
                                 if(DataElement3->SDP_Data_Element_Type == deUnsignedInteger2Bytes)
                                    SDPRecordInfo.Protocol[SDPRecordInfo.NumProtocols].Value = DataElement3->SDP_Data_Element.UnsignedInteger2Bytes;
                                 else
                                    SDPRecordInfo.Protocol[SDPRecordInfo.NumProtocols].Value = DataElement3->SDP_Data_Element.UnsignedInteger1Byte;
                              }
                           }

                           /* Increment the number of protocols that    */
                           /* have been processed.                      */
                           SDPRecordInfo.NumProtocols++;
                        }
                        DataElement2++;
                     }
                  }
                  break;
               case SDP_ATTRIBUTE_ID_ADDITIONAL_PROTOCOL_DESCRIPTOR_LISTS:
                  /* Verify that the data element is what we expected.  */
                  if(DataElement1->SDP_Data_Element_Type == deSequence)
                  {
                     /* The Additional Protocol Descriptor List is a    */
                     /* sequence of Additional Protocol Descriptor      */
                     /* Lists.  Since we are only going to process the  */
                     /* 1st list, then increment to the first list      */
                     /* sequence.                                       */
                     DataElement1 = DataElement1->SDP_Data_Element.SDP_Data_Element_Sequence;
                     if(DataElement1->SDP_Data_Element_Type == deSequence)
                     {
                        DataElement2 = DataElement1->SDP_Data_Element.SDP_Data_Element_Sequence;
                        Index        = DataElement1->SDP_Data_Element_Length;
                        while((Index--) && (SDPRecordInfo.NumAdditionalProtocols < MAX_SDP_RECORD_ARRAY_LIST))
                        {
                           /* Verify that this element is a Data Element*/
                           /* Sequence.                                 */
                           if(DataElement2->SDP_Data_Element_Type == deSequence)
                           {
                              /* Get the start of the next sequence.    */
                              DataElement3 = DataElement2->SDP_Data_Element.SDP_Data_Element_Sequence;

                              /* Extract a UUID_16_t from the Data      */
                              /* Element                                */
                              SDPRecordInfo.AdditionalProtocol[SDPRecordInfo.NumAdditionalProtocols].UUID_ID = ExtractUUID_ID(DataElement3);

                              /* If the Length was greater than 1, then */
                              /* there is an additional Byte or Word    */
                              /* that follows.                          */
                              if(DataElement2->SDP_Data_Element_Length > 1)
                              {
                                 /* Advance to the next element and get */
                                 /* the value that follows.             */
                                 DataElement3++;

                                 if(SDPRecordInfo.NumAdditionalProtocols < MAX_SDP_RECORD_ARRAY_LIST)
                                 {
                                    if(DataElement3->SDP_Data_Element_Type == deUnsignedInteger2Bytes)
                                       SDPRecordInfo.AdditionalProtocol[SDPRecordInfo.NumAdditionalProtocols].Value = DataElement3->SDP_Data_Element.UnsignedInteger2Bytes;
                                    else
                                       SDPRecordInfo.AdditionalProtocol[SDPRecordInfo.NumAdditionalProtocols].Value = DataElement3->SDP_Data_Element.UnsignedInteger1Byte;
                                 }
                              }

                              /* Increment the number of protocols that */
                              /* have been processed.                   */
                              SDPRecordInfo.NumAdditionalProtocols++;
                           }
                           DataElement2++;
                        }
                     }
                  }
                  break;
               case SDP_ATTRIBUTE_ID_BLUETOOTH_PROFILE_DESCRIPTOR_LIST:
                  /* Verify that the data element is what we expected.  */
                  if(DataElement1->SDP_Data_Element_Type == deSequence)
                  {
                     /* The Profile Descriptor List is a sequence of    */
                     /* Data Element sequences, where the 1st element in*/
                     /* each sequence identifies the Profile followed by*/
                     /* the Profile Version.                            */
                     DataElement2 = DataElement1->SDP_Data_Element.SDP_Data_Element_Sequence;
                     Index        = DataElement1->SDP_Data_Element_Length;
                     while((Index--) && (SDPRecordInfo.NumProfiles < MAX_SDP_RECORD_ARRAY_LIST))
                     {
                        /* Verify that this element is a Data Element   */
                        /* Sequence.                                    */
                        if(DataElement2->SDP_Data_Element_Type == deSequence)
                        {
                           /* Get the start of the next sequence.       */
                           DataElement3 = DataElement2->SDP_Data_Element.SDP_Data_Element_Sequence;

                           /* Extract a UUID_16_t from the Data Element */
                           SDPRecordInfo.Profile[SDPRecordInfo.NumProfiles].UUID_ID = ExtractUUID_ID(DataElement3);

                           /* Verify that the version information is    */
                           /* present.                                  */
                           if(DataElement2->SDP_Data_Element_Length > 1)
                           {
                              /* Advance to the next element and get the*/
                              /* value that follows.                    */
                              DataElement3++;

                              if(SDPRecordInfo.NumProfiles < MAX_SDP_RECORD_ARRAY_LIST)
                              {
                                 if(DataElement3->SDP_Data_Element_Type == deUnsignedInteger2Bytes)
                                    SDPRecordInfo.Profile[SDPRecordInfo.NumProfiles].Value = DataElement3->SDP_Data_Element.UnsignedInteger2Bytes;
                              }
                           }

                           /* Increment the number of profiles that have*/
                           /* been processed.                           */
                           SDPRecordInfo.NumProfiles++;
                        }
                        DataElement2++;
                     }
                  }
                  break;
               case (SDP_ATTRIBUTE_ID_PRIMARY_LANGUAGE_BASE_VALUE+SDP_ATTRIBUTE_OFFSET_ID_SERVICE_NAME):
                  /* Verify that the data element is what we expected.  */
                  if(DataElement1->SDP_Data_Element_Type == deTextString)
                  {
                     SDPRecordInfo.ServiceName       = DataElement1->SDP_Data_Element.TextString;
                     SDPRecordInfo.ServiceNameLength = (Word_t)DataElement1->SDP_Data_Element_Length;
                  }
                  break;
               case (SDP_ATTRIBUTE_ID_PRIMARY_LANGUAGE_BASE_VALUE+SDP_ATTRIBUTE_OFFSET_ID_SERVICE_DESCRIPTION):
                  /* Verify that the data element is what we expected.  */
                  if(DataElement1->SDP_Data_Element_Type == deTextString)
                  {
                     SDPRecordInfo.ServiceDesc       = DataElement1->SDP_Data_Element.TextString;
                     SDPRecordInfo.ServiceDescLength = (Word_t)DataElement1->SDP_Data_Element_Length;
                  }
                  break;
               case (SDP_ATTRIBUTE_ID_PRIMARY_LANGUAGE_BASE_VALUE+SDP_ATTRIBUTE_OFFSET_ID_PROVIDER_NAME):
                  /* Verify that the data element is what we expected.  */
                  if(DataElement1->SDP_Data_Element_Type == deTextString)
                  {
                     SDPRecordInfo.ProviderName       = DataElement1->SDP_Data_Element.TextString;
                     SDPRecordInfo.ProviderNameLength = (Word_t)DataElement1->SDP_Data_Element_Length;
                  }
                  break;
               default:
                  break;
            }
         }

         ret_val = ProcessHeadsetServiceRecord(&SDPRecordInfo, ProfileInfo);
      }
   }

   return(ret_val);
}


/* The following function is used to extract a UUID value and convert*/
/* it to a Word Value that represents the 16 Bit UUID value.         */
Word_t ExtractUUID_ID(SDP_Data_Element_t *DataElementPtr)
{
   Word_t UUID = 0;

   /* Verify that the value passed in appears valid.                    */
   if(DataElementPtr)
   {
      /* Verify that this parameter is a UUID.                          */
      switch(DataElementPtr->SDP_Data_Element_Type)
      {
         case deUUID_128:
            UUID = (Word_t)((DataElementPtr->SDP_Data_Element.UUID_128.UUID_Byte2 << 8) + DataElementPtr->SDP_Data_Element.UUID_128.UUID_Byte3);
            break;
         case deUUID_32:
            UUID = (Word_t)((DataElementPtr->SDP_Data_Element.UUID_32.UUID_Byte2 << 8) + DataElementPtr->SDP_Data_Element.UUID_32.UUID_Byte3);
            break;
         case deUUID_16:
            UUID = (Word_t)((DataElementPtr->SDP_Data_Element.UUID_16.UUID_Byte0 << 8) + DataElementPtr->SDP_Data_Element.UUID_16.UUID_Byte1);
         default:
            break;
      }
   }

   return(UUID);
}


/* The following function is used to process the information that has*/
/* been extracted from one SDP Service Record.  The information will */
/* be examined to determine if the record is a Headset profile       */
/* record.  If a Headset record is located, the information is       */
/* extracted from the SDP Record and placed into the appropriate     */
/* fields of the Profile Information structure.                      */
Boolean_t ProcessHeadsetServiceRecord(SDP_Record_t *SDPRecordPtr, HDS_Profile_Info_t *ProfileInfoPtr)
{
   Boolean_t ret_val = FALSE;

   /* Verify that the parameter passed in appears valid.                */
   if((SDPRecordPtr) && (ProfileInfoPtr))
   {
      /* Initialize the Profile Info Structure.                         */
      BTPS_MemInitialize(ProfileInfoPtr, 0, PROFILE_INFO_DATA_SIZE);

      /* Determine the Profile that is defined by this record.          */
      ret_val = ParseAudioGatewayServiceClass(SDPRecordPtr);
      if(ret_val)
      {
         /* Set the information that is common to all profiles.         */
         ProfileInfoPtr->ServiceDescLength     = SDPRecordPtr->ServiceDescLength;
         ProfileInfoPtr->ServiceDesc           = SDPRecordPtr->ServiceDesc;
         ProfileInfoPtr->ServiceNameLength     = SDPRecordPtr->ServiceNameLength;
         ProfileInfoPtr->ServiceName           = SDPRecordPtr->ServiceName;
         ProfileInfoPtr->ServiceProviderLength = SDPRecordPtr->ProviderNameLength;
         ProfileInfoPtr->ServiceProvider       = SDPRecordPtr->ProviderName;

         /* Verify that there are at lease 2 protocol entries and that  */
         /* the second protocol indicates RFCOMM.                       */
         if((SDPRecordPtr->NumProtocols >= 2) && (SDPRecordPtr->Protocol[1].UUID_ID == RFCOMM_PROTOCOL_UUID))
         {
            ProfileInfoPtr->HDSInfo.ServerChannel = (Byte_t)SDPRecordPtr->Protocol[1].Value;

            /* Verify that there are at lease 1 Profile entries.        */
            if((SDPRecordPtr->NumProfiles >= 1) && ((SDPRecordPtr->Profile[0].UUID_ID == HEADSET_PROFILE_HEADSET_UUID) || (SDPRecordPtr->Profile[0].UUID_ID == HEADSET_PROFILE_AUDIO_GATEWAY_UUID)))
            {
               ProfileInfoPtr->HDSInfo.ProfileVersion = SDPRecordPtr->Profile[0].Value;
            }
            else
               ret_val = FALSE;
         }
         else
            ret_val = FALSE;
      }
   }

   return(ret_val);
}


/* The following function is used to scan the information that was   */
/* extracted from an SDP Service Record and determine if this is a   */
/* Headset Audio Gateway Service Record.                             */
Boolean_t ParseAudioGatewayServiceClass(SDP_Record_t *SDPRecordPtr)
{
   Word_t       *ServiceClass;
   Boolean_t     ret_val = FALSE;
   unsigned int  Index1;

   /* Verify that the parameter passed in appears valid.                */
   if(SDPRecordPtr)
   {
      /* Scan through the Service Class UUIDs that we found.            */
      Index1       = SDPRecordPtr->NumServiceClass;
      ServiceClass = SDPRecordPtr->ServiceClass;

      while((Index1--) && (!ret_val))
      {
         /* Check for a Service Class match of the profile.             */
         if(*ServiceClass == HEADSET_PROFILE_HEADSET_UUID)
            ret_val = TRUE;

         ServiceClass++;
      }
   }

   return(ret_val);
}


/* The following function is responsible for setting the Speaker Gain*/
/* on Remote Device.  This function returns zero on successful       */
/* execution and a negative value on all errors.                     */
int HDSMSetRemoteSpeakerGain(ParameterList_t *TempParam)
{
   int       Result;
   int       ret_val;
   BD_ADDR_t BD_ADDR;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* Verify that the HDS Event Callback has been registered.        */
      if(HDSventCallbackID)
      {
         /* The Port ID appears to be semi-valid, now check the Speaker */
         /* Gain.                                                       */
         if((TempParam) && (TempParam->NumberofParameters > 1) && (TempParam->Params[1].intParam <= HDSET_SPEAKER_GAIN_MAXIMUM))
         {
            /* Convert the parameter to a Bluetooth Device Address.     */
            StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

            /* The Speaker Gain is a valid value.  Now submit the       */
            /* command.                                                 */
            Result  = HDSM_Set_Remote_Speaker_Gain(HDSventCallbackID, sctAudioGateway, BD_ADDR, TempParam->Params[1].intParam);

            /* Set the return value of this function equal to the Result*/
            /* of the function call.                                    */
            ret_val = Result;

            if(!Result)
            {
               /* The function was submitted successfully.              */
               printf("HDS_Set_Remote_Speaker_Gain: Function Successful.\r\n");
            }
            else
            {
               /* There was an error submitting the function.           */
               printf("HDSM_Set_Remote_Voice_Recognition_Activation() Failure: %d (%s).\r\n", Result, ERR_ConvertErrorCodeToString(Result));
            }
         }
         else
         {
            /* The Speaker Gain parameter is invalid.                   */
            printf("Usage: SetSpeakerGain [BD_ADDR] [0 <= SpeakerGain <= 15].\r\n");

            ret_val = INVALID_PARAMETERS_ERROR;
         }
      }
      else
      {
         /* One or more of the necessary parameters is/are invalid.     */
         printf("HDS Event Callback MUST be registered before making this call.\r\n");

         ret_val = INVALID_PARAMETERS_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for setting the Microphone  */
/* Gain on Remote Device.  This function returns zero on successful  */
/* execution and a negative value on all errors.                     */
int HDSMSetRemoteMicrophoneGain(ParameterList_t *TempParam)
{
   int       Result;
   int       ret_val;
   BD_ADDR_t BD_ADDR;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* Verify that the HDS Event Callback has been registered.        */
      if(HDSventCallbackID)
      {
         /* The Port ID appears to be semi-valid, now check the         */
         /* Microphone Gain.                                            */
         if((TempParam) && (TempParam->NumberofParameters > 1) && (TempParam->Params[1].intParam <= HDSET_MICROPHONE_GAIN_MAXIMUM))
         {
            /* Convert the parameter to a Bluetooth Device Address.     */
            StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

            /* The Microphone Gain is a valid value.  Now submit the    */
            /* command.                                                 */
            Result  = HDSM_Set_Remote_Microphone_Gain(HDSventCallbackID, sctAudioGateway, BD_ADDR, TempParam->Params[1].intParam);

            /* Set the return value of this function equal to the Result*/
            /* of the function call.                                    */
            ret_val = Result;

            if(!Result)
            {
               /* The function was submitted successfully.              */
               printf("HDSM_Set_Remote_Microphone_Gain(): Function Successful.\r\n");
            }
            else
            {
               /* There was an error submitting the function.           */
               printf("HDSM_Set_Remote_Microphone_Gain() Failure: %d (%s).\r\n", Result, ERR_ConvertErrorCodeToString(Result));
            }
         }
         else
         {
            /* The Microphone Gain parameter is invalid.                */
            printf("Usage: SetMicrophoneGain [BD_ADDR] [0 <= MicrophoneGain <= 15].\r\n");

            ret_val = INVALID_PARAMETERS_ERROR;
         }
      }
      else
      {
         /* One or more of the necessary parameters is/are invalid.     */
         printf("HDS Event Callback MUST be registered before making this call.\r\n");

         ret_val = INVALID_PARAMETERS_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


int write_SCO_Configuration(ParameterList_t *TempParam)
{
    int ret_val;
    int counter;
    Byte_t CommandBuffer[5];
    Byte_t CommandLength;
    Word_t OCF;
    Byte_t OGF;
    Byte_t Length;
    Byte_t Status;
    Byte_t ReturnBuffer[16];

    /* First, check to make sure that we have already been initialized.  */
    if(Initialized)
    {
       /* Next, check to make sure that a valid Data format was          */
       /* specified.                                                     */
       if((TempParam) && (TempParam->NumberofParameters == 2) && (TempParam->Params[0].intParam > 0) && (TempParam->Params[1].intParam > 0))
       {
          /* Parameters appear to be valid. */
          ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[0], 1);                            // 0: Codec; 1: Voice over HCI
          ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[1], TempParam->Params[0].intParam);// Delay (in byte)
          ASSIGN_HOST_WORD_TO_LITTLE_ENDIAN_UNALIGNED_WORD(&CommandBuffer[2], TempParam->Params[1].intParam);// Latency (in bytes)
          ASSIGN_HOST_BYTE_TO_LITTLE_ENDIAN_UNALIGNED_BYTE(&CommandBuffer[4], 1);                            // Accept packet with bad CRC

          CommandLength    = sizeof(CommandBuffer);
          Length           = sizeof(ReturnBuffer);
          OGF              = VS_COMMAND_OGF(VS_SCO_CONFIGURATION_COMMAND_ADDRESS);
          OCF              = VS_COMMAND_OCF(VS_SCO_CONFIGURATION_COMMAND_ADDRESS);

          ret_val = DEVM_SendRawHCICommand(OGF, OCF, CommandLength, CommandBuffer, &Status, &Length, ReturnBuffer, TRUE);
          printf("EnableSCOOverHCI(%d, %d): %s (%d) -",
                                  TempParam->Params[0].intParam,
                                  TempParam->Params[1].intParam,
                                  ((ret_val)?"FAIL":"OK"),
                                  ret_val);
          for (counter = 0; counter < Length; counter++) printf(" %02X", (((unsigned int)(ReturnBuffer[counter]))&0xFF));
          printf(")\n");
       }
       else
       {
          /* Invalid parameters specified so flag an error to the user.  */
          printf("Usage: EnableSCOOverHCI [Size] [Latency].\r\n");

          /* Flag that invalid parameters were specified.                */
          ret_val = INVALID_PARAMETERS_ERROR;
       }
    }
    else
    {
       /* No valid Bluetooth Stack ID exists.                            */
       ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
    }

    return(ret_val);
}


/* The following function is responsible for setting up or releasing */
/* an audio connection.  This function returns zero on successful    */
/* execution and a negative value on all errors.                     */
int ManageAudioConnection(ParameterList_t *TempParam)
{
   int       ret_val;
   BD_ADDR_t BD_ADDR;

   /* Verify that the HDS Event Callback has been registered.           */
   if(HDSventCallbackID)
   {
      /* make sure the passed in parameters appears to be semi-valid.   */
      if((TempParam) && (TempParam->NumberofParameters > 1))
      {
         /* Convert the parameter to a Bluetooth Device Address.        */
         StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

         /* Check to see if this is a request to setup an audio         */
         /* connection or disconnect an audio connection.               */
         if(TempParam->Params[1].intParam)
         {
            /* Verify that the in band ringing parameter is specified.  */
            if(TempParam->NumberofParameters > 2)
            {
               /* This is a request to setup an audio connection, call  */
               /* the Setup Audio Connection function.                  */
               ret_val = HDSMSetupAudioConnection(BD_ADDR, (Boolean_t)TempParam->Params[2].intParam);
            }
            else
            {
               /* The required parameter is invalid.                    */
               printf("Usage: ManageAudio [BD_ADDR] [Release = 0, Setup = 1] "
                      "[InBandRinging (0 = No, 1 = In Band Ringing) only valid if Setup = 1].\r\n");

               ret_val = INVALID_PARAMETERS_ERROR;
            }
         }
         else
         {
            /* This is a request to disconnect an audio connection, call*/
            /* the Release Audio Connection function.                   */
            ret_val = HDSMReleaseAudioConnection(BD_ADDR);
         }
      }
      else
      {
         /* The required parameter is invalid.                          */
         printf("Usage: ManageAudio [BD_ADDR] [Release = 0, Setup = 1] [InBandRinging (0 = No, 1 = In Band Ringing) only valid if Setup = 1].\r\n");

         ret_val = INVALID_PARAMETERS_ERROR;
      }
   }
   else
   {
      /* One or more of the necessary parameters is/are invalid.        */
      printf("HDS Event Callback MUST be registered before making this call.\r\n");

      ret_val = INVALID_PARAMETERS_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for Querying the Remote     */
/* Device Services of the specified Remote Device.  This function    */
/* returns zero if successful and a negative value if an error       */
/* occurred.                                                         */
int QueryHeadsetServices(ParameterList_t *TempParam)
{
   int       ret_val;
   BD_ADDR_t BD_ADDR;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* Make sure that all of the parameters required for this function*/
      /* appear to be at least semi-valid.                              */
      if((TempParam) && (TempParam->NumberofParameters > 1))
      {
         /* Initialize success.                                         */
         ret_val = 0;

         /* Convert the parameter to a Bluetooth Device Address.        */
         StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

         printf("Attempting Query Remote Device %s For Services.\r\n", TempParam->Params[0].strParam);

         ret_val = GetHeadsetServiceInformation(BD_ADDR, TempParam->Params[1].intParam, NULL);
      }
      else
      {
         /* One or more of the necessary parameters is/are invalid.     */
         printf("Usage: QueryHeadsetServices [BD_ADDR] [Force Update].\r\n");

         ret_val = INVALID_PARAMETERS_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for Setting up an Audio     */
/* Connection.  This function returns zero on successful execution   */
/* and a negative value on all errors.                               */
int HDSMSetupAudioConnection(BD_ADDR_t BD_ADDR, Boolean_t InBandRinging)
{
    int Result;
    int ret_val;

    /* First, check to make sure that we have already been initialized.  */
    if(Initialized)
    {
       /* The Port ID appears to be a semi-valid value.  Now submit the  */
       /* command.                                                       */
       Result  = HDSM_Setup_Audio_Connection(HDSventCallbackID, sctAudioGateway, BD_ADDR, InBandRinging);

       /* Set the return value of this function equal to the Result of   */
       /* the function call.                                             */
       ret_val = Result;

       /* Now check to see if the command was submitted successfully.    */
       if(!Result)
       {
          /* The function was submitted successfully.                    */
          printf("HDSM_Setup_Audio_Connection: Function Successful.\r\n");
       }
       else
       {
          /* There was an error submitting the function.                 */
          printf("HDSM_Setup_Audio_Connection() Failure: %d.\r\n", Result);
       }
    }
    else
    {
       /* Not initialized, flag an error.                                */
       printf("Platform Manager has not been initialized.\r\n");

       ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
    }

    return(ret_val);
}


/* The following function is responsible for Querying the Remote     */
/* Device Properties of a specific Remote Device.  This function     */
/* returns zero if successful and a negative value if an error       */
/* occurred.                                                         */
int QueryRemoteDeviceProperties(ParameterList_t *TempParam)
{
   int                             Result;
   int                             ret_val;
   BD_ADDR_t                       BD_ADDR;
   Boolean_t                       ForceUpdate;
   DEVM_Remote_Device_Properties_t RemoteDeviceProperties;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* Make sure that all of the parameters required for this function*/
      /* appear to be at least semi-valid.                              */
      if((TempParam) && (TempParam->NumberofParameters))
      {
         /* Convert the parameter to a Bluetooth Device Address.        */
         StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

         if(TempParam->NumberofParameters > 1)
            ForceUpdate = (Boolean_t)(TempParam->Params[1].intParam?TRUE:FALSE);
         else
            ForceUpdate = FALSE;

         printf("Attempting to Query Device Properties: %s, ForceUpdate: %s.\r\n", TempParam->Params[0].strParam, ForceUpdate?"TRUE":"FALSE");

         if((Result = DEVM_QueryRemoteDeviceProperties(BD_ADDR, ForceUpdate, &RemoteDeviceProperties)) >= 0)
         {
            printf("DEVM_QueryRemoteDeviceProperties() Success: %d.\r\n", Result);

            /* Display the Remote Device Properties.                    */
            DisplayRemoteDeviceProperties(0, &RemoteDeviceProperties);

            /* Flag success.                                            */
            ret_val = 0;
         }
         else
         {
            /* Error Querying Remote Device, inform the user and flag an*/
            /* error.                                                   */
            printf("DEVM_QueryRemoteDeviceProperties() Failure: %d, %s.\r\n", Result, ERR_ConvertErrorCodeToString(Result));

            ret_val = FUNCTION_ERROR;
         }
      }
      else
      {
         /* One or more of the necessary parameters is/are invalid.     */
         printf("Usage: QueryRemoteDeviceProperties [BD_ADDR] [Force Update].\r\n");

         ret_val = INVALID_PARAMETERS_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for disconnecting a Headset */
/* connection.  This function returns zero on successful execution   */
/* and a negative value on all errors.                               */
int HDSMDisconnectDevice(ParameterList_t *TempParam)
{
   int       Result;
   int       ret_val;
   BD_ADDR_t BD_ADDR;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* Verify that the HDS Event Callback has been registered.        */
      if(HDSventCallbackID)
      {
         /* The port ID appears to be at least semi-valid, now check to */
         /* make sure the passed in parameters appears to be semi-valid.*/
         if((TempParam) && (TempParam->NumberofParameters > 0))
         {
            /* Convert the parameter to a Bluetooth Device Address.     */
            StrToBD_ADDR(TempParam->Params[0].strParam, &BD_ADDR);

            /* Disconnect the Device.                                   */
            Result = HDSM_Disconnect_Device(sctAudioGateway, BD_ADDR);

            /* Set the return value of this function equal to the Result*/
            /* of the function call.                                    */
            ret_val = Result;

            if(!Result)
            {
               /* The function was submitted successfully.              */
               printf("HDSM_Disconnect_Device: Function Successful.\r\n");
            }
            else
            {
               /* There was an error submitting the function.           */
               printf("HDSM_Disconnect_Device() Failure: %d (%s).\r\n", Result, ERR_ConvertErrorCodeToString(Result));
            }
         }
         else
         {
            /* One or more of the necessary parameters is/are invalid.  */
            printf("Usage: Disconnect [BD_ADDR].\r\n");

            ret_val = INVALID_PARAMETERS_ERROR;
         }
      }
      else
      {
         /* One or more of the necessary parameters is/are invalid.     */
         printf("HDS Event Callback MUST be registered before making this call.\r\n");

         ret_val = INVALID_PARAMETERS_ERROR;
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}


/* The following function is responsible for Releasing an existing   */
/* Audio Connection.  This function returns zero on successful       */
/* execution and a negative value on all errors.                     */
int HDSMReleaseAudioConnection(BD_ADDR_t BD_ADDR)
{
   int Result;
   int ret_val;

   /* First, check to make sure that we have already been initialized.  */
   if(Initialized)
   {
      /* The Port ID appears to be a semi-valid value.  Now submit the  */
      /* command.                                                       */
      Result  = HDSM_Release_Audio_Connection(HDSventCallbackID, sctAudioGateway, BD_ADDR);

      /* Set the return value of this function equal to the Result of   */
      /* the function call.                                             */
      ret_val = Result;

      /* Now check to see if the command was submitted successfully.    */
      if(!Result)
      {
         /* The function was submitted successfully.                    */
         printf("HDSM_Release_Audio_Connection: Function Successful.\r\n");
      }
      else
      {
         /* There was an error submitting the function.                 */
         printf("HDSM_Release_Audio_Connection() Failure: %d.\r\n", Result);
      }
   }
   else
   {
      /* Not initialized, flag an error.                                */
      printf("Platform Manager has not been initialized.\r\n");

      ret_val = PLATFORM_MANAGER_NOT_INITIALIZED_ERROR;
   }

   return(ret_val);
}
