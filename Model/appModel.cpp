#include <qglobal.h>
#include <unistd.h>

#include "appModel.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif


AppModel::AppModel(const QString &deviceAdd) :
    AbstractVisionARModel(),
    status   { AppStatus::WELCOME },
    deviceAdd{ deviceAdd },
    btManager{ nullptr }
{
}


AppModel::~AppModel()
{
    if (btManager != nullptr)
    {
        delete btManager;
        btManager = nullptr;
    }
}


void AppModel::runModel()
{
    emit showWindow(FIRST_WINDOW);
}


void AppModel::backReleased(int /*timePressed*/)
{
    switch (status)
    {
    case AppStatus::WELCOME:
        emit newAlert(1, "QuestionCloseApp", "Return to the Launcher?");

        break;

    case AppStatus::CONNECTION_INFO:
        emit showWindow(FIRST_WINDOW);
        status = AppStatus::WELCOME;

        break;

    case AppStatus::PAIRING:   [[fallthrough]];
    case AppStatus::CONNECTED:

        if (btManager != nullptr)
        {
            btManager->QueryRemoteDeviceProperty();
            if (!btManager->ManageAudio(0))
            {
                sleep(2);
                btManager->Disconnect();
            }

            delete btManager;
            btManager = nullptr;
        }

        emit showWindow(SECOND_WINDOW);
        status = AppStatus::CONNECTION_INFO;

        break;

    default:
        break;
    }
}


void AppModel::okPressed()
{
    switch (status)
    {
    case AppStatus::WELCOME:
        emit showWindow(SECOND_WINDOW);
        status = AppStatus::CONNECTION_INFO;

        break;

    case AppStatus::CONNECTION_INFO:
        emit showWindow(THIRD_WINDOW);
        status = AppStatus::PAIRING;

        btManager = new BluetoothManager(deviceAdd);

        btManager->QueryRemoteDeviceService();

        if (!btManager->ConnectDevice())
        {
            sleep(5);

            if (!btManager->RingIndication())
            {
                emit showWindow(FOURTH_WINDOW);
                status = AppStatus::CONNECTED;

                btManager->SetSpeakerGain();
                sleep(2);
                btManager->SetMicrophoneGain();
                sleep(2);

                if (!btManager->Write_SCO_Configuration())
                {
                    sleep(2);

                    if (!btManager->ManageAudio(1))
                    {
                        btManager->QueryHeadsetService();
                    }
                }
            }
        }

        break;

    case AppStatus::PAIRING:

        break;

    case AppStatus::CONNECTED:

        break;

    default:
        break;
    }
}


void AppModel::manageAlertsEnd(QString id, bool value)
{
    if (status == AppStatus::WELCOME &&
            id == "QuestionCloseApp" &&
            value)
    {
        emit endApplication();
    }
}
