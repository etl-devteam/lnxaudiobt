#ifndef BLUETOPIA_H
#define BLUETOPIA_H


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>

#include <unistd.h>        /* Include for getpid().                              */
#include <fcntl.h>

extern "C" {
#include "SS1BTHDSM.h"     /* HDS Manager Application Programming Interface.     */
#include "SS1BTPM.h"       /* BTPM Application Programming Interface.            */
#include "BTPMVSAPI.h"	   /* BTVS Vendor Specific commands to configure the PCM */
} // extern "C"

#define HEADSET_PROFILE_HEADSET_UUID                  0x1108
#define HEADSET_PROFILE_AUDIO_GATEWAY_UUID            0x1112
#define RFCOMM_PROTOCOL_UUID                          0x0003
#define SDP_ATTRIBUTE_ID_PRIMARY_LANGUAGE_BASE_VALUE  0x100

#define MAX_SUPPORTED_COMMANDS                     (51)  /* Denotes the       */
                                                         /* maximum number of */
                                                         /* User Commands that*/
                                                         /* are supported by  */
                                                         /* this application. */

#define MAX_COMMAND_LENGTH                        (256)  /* Denotes the max   */
                                                         /* buffer size used  */
                                                         /* for user commands */
                                                         /* input via the     */
                                                         /* User Interface.   */

#define MAX_NUM_OF_PARAMETERS                       (5)  /* Denotes the max   */
                                                         /* number of         */
                                                         /* parameters a      */
                                                         /* command can have. */

#define DEFAULT_IO_CAPABILITY          (icDisplayYesNo)  /* Denotes the       */
                                                         /* default I/O       */
                                                         /* Capability that is*/
                                                         /* used with Secure  */
                                                         /* Simple Pairing.   */

#define DEFAULT_MITM_PROTECTION                  (TRUE)  /* Denotes the       */
                                                         /* default value used*/
                                                         /* for Man in the    */
                                                         /* Middle (MITM)     */
                                                         /* protection used   */
                                                         /* with Secure Simple*/
                                                         /* Pairing.          */

#define INDENT_LENGTH                                 3  /* Denotes the number*/
                                                         /* of character      */
                                                         /* spaces to be used */
                                                         /* for indenting when*/
                                                         /* displaying SDP    */
                                                         /* Data Elements.    */


#define NO_COMMAND_ERROR                           (-1)  /* Denotes that no   */
                                                         /* command was       */
                                                         /* specified to the  */
                                                         /* parser.           */

#define INVALID_COMMAND_ERROR                      (-2)  /* Denotes that the  */
                                                         /* Command does not  */
                                                         /* exist for         */
                                                         /* processing.       */

#define EXIT_CODE                                  (-3)  /* Denotes that the  */
                                                         /* Command specified */
                                                         /* was the Exit      */
                                                         /* Command.          */

#define FUNCTION_ERROR                             (-4)  /* Denotes that an   */
                                                         /* error occurred in */
                                                         /* execution of the  */
                                                         /* Command Function. */

#define TO_MANY_PARAMS                             (-5)  /* Denotes that there*/
                                                         /* are more          */
                                                         /* parameters then   */
                                                         /* will fit in the   */
                                                         /* UserCommand.      */

#define INVALID_PARAMETERS_ERROR                   (-6)  /* Denotes that an   */
                                                         /* error occurred due*/
                                                         /* to the fact that  */
                                                         /* one or more of the*/
                                                         /* required          */
                                                         /* parameters were   */
                                                         /* invalid.          */

#define PLATFORM_MANAGER_NOT_INITIALIZED_ERROR     (-7)  /* Denotes that an   */
                                                         /* error occurred due*/
                                                         /* to the fact that  */
                                                         /* the Platform      */
                                                         /* Manager has not   */
                                                         /* been initialized. */

#define MAX_NUMBER_CALL_ENTRIES                    (20)  /* Constant which    */
                                                         /* determines the    */
                                                         /* maximum number of */
                                                         /* calls a user can  */
                                                         /* input into the    */
                                                         /* Send Call List    */
                                                         /* function.         */

#define CMD_LINE_FLAGS_EXEC_INIT 	0x00000001           /* Flags for Init    */
#define CMD_LINE_FLAGS_SET_DEBUG 	0x00000002			 /* function to init  */
                                                         /* the HSP with debug*/

/* The following typedef represents a buffer large enough to hold a  */
/* connection type string.                                           */
typedef char ConnectionTypeStr_t[16];


/* The following type definition represents the structure which holds*/
/* all information about the parameter, in particular the parameter  */
/* as a string and the parameter as an unsigned int.                 */
typedef struct _tagParameter_t
{
    char         *strParam;
    unsigned int  intParam;
} Parameter_t;


/* The following type definition represents the structure which holds*/
/* a list of parameters that are to be associated with a command The */
/* NumberofParameters variable holds the value of the number of      */
/* parameters in the list.                                           */
typedef struct _tagParameterList_t
{
    int         NumberofParameters;
    Parameter_t Params[MAX_NUM_OF_PARAMETERS];
} ParameterList_t;


/* The following type definition represents the structure which holds*/
/* the command and parameters to be executed.                        */
typedef struct _tagUserCommand_t
{
    char            *Command;
    ParameterList_t  Parameters;
} UserCommand_t;

/* The following type definition represents the generic function     */
/* pointer to be used by all commands that can be executed by the    */
/* test program.                                                     */
typedef int (*CommandFunction_t)(ParameterList_t *TempParam);

/* The following type definition represents the structure which holds*/
/* information used in the interpretation and execution of Commands. */
typedef struct _tagCommandTable_t
{
    char              *CommandName;
    CommandFunction_t  CommandFunction;
} CommandTable_t;

#define MAX_SDP_RECORD_ARRAY_LIST      20

/* Holds information on a sequence.                                  */
typedef struct _tagSeq_Info_t
{
    Word_t UUID_ID;
    Word_t Value;
} Seq_Info_t;

/* The following structure is used to hold information located in a  */
/* single SDP Service Record.  The structure is tailored to hold     */
/* information about the profiles that are supported with this       */
/* version of DISC.                                                  */
typedef struct _tagSDP_Record_t
{
    Byte_t     NumServiceClass;
    Word_t     ServiceClass[MAX_SDP_RECORD_ARRAY_LIST];
    Byte_t     NumProtocols;
    Seq_Info_t Protocol[MAX_SDP_RECORD_ARRAY_LIST];
    Byte_t     NumAdditionalProtocols;
    Seq_Info_t AdditionalProtocol[MAX_SDP_RECORD_ARRAY_LIST];
    Byte_t     NumProfiles;
    Seq_Info_t Profile[MAX_SDP_RECORD_ARRAY_LIST];
    Word_t     ServiceNameLength;
    Byte_t    *ServiceName;
    Word_t     ServiceDescLength;
    Byte_t    *ServiceDesc;
    Word_t     ProviderNameLength;
    Byte_t    *ProviderName;
} SDP_Record_t;

/* The following defines the SDP Information that pertains to the    */
/* Headset Profile.                                                  */
typedef struct _tagHDS_Info_t
{
    Byte_t ServerChannel;
    Word_t ProfileVersion;
} HDS_Info_t;

/* The following defines the structure that contains information     */
/* about a specific profile supported on the remote device.  The     */
/* Profile Identifier value identifies the Profile Information       */
/* structure that is used to access the information about the        */
/* profile.                                                          */
typedef struct _tagProfile_Info_t
{
    Word_t       ServiceNameLength;
    Byte_t      *ServiceName;
    Word_t       ServiceDescLength;
    Byte_t      *ServiceDesc;
    Word_t       ServiceProviderLength;
    Byte_t      *ServiceProvider;
    HDS_Info_t  HDSInfo;
} HDS_Profile_Info_t;

#define PROFILE_INFO_DATA_SIZE                  (sizeof(HDS_Profile_Info_t))

/* Internal function prototypes.                                     */
void UserInterface(void);
int ReadLine(char *Buffer, unsigned int BufferSize);
unsigned int StringToUnsignedInteger(char *StringInteger);
char *StringParser(char *String);
int CommandParser(UserCommand_t *TempCommand, char *UserInput);
int CommandInterpreter(UserCommand_t *TempCommand);
int AddCommand(char *CommandName, CommandFunction_t CommandFunction);
CommandFunction_t FindCommand(char *Command);
void ClearCommands(void);

void BD_ADDRToStr(BD_ADDR_t Board_Address, char *BoardStr);

void ConnectionTypeToStr(HDSM_Connection_Type_t ConnectionType, ConnectionTypeStr_t ConnectionTypeStr);
void StrToBD_ADDR(char *BoardStr, BD_ADDR_t *Board_Address);

int DisplayHelp(ParameterList_t *TempParam);

int Initialize(ParameterList_t *TempParam);
int EnableBluetoothDebug(ParameterList_t *TempParam);
int Cleanup(ParameterList_t *TempParam);
int RegisterEventCallback(ParameterList_t *TempParam);
int UnRegisterEventCallback(ParameterList_t *TempParam);
int SetDevicePower(ParameterList_t *TempParam);
int QueryDevicePower(ParameterList_t *TempParam);
int SetLocalRemoteDebugZoneMask(ParameterList_t *TempParam);
int QueryLocalRemoteDebugZoneMask(ParameterList_t *TempParam);
int ShutdownService(ParameterList_t *TempParam);
int QueryLocalDeviceProperties(ParameterList_t *TempParam);
int SetLocalDeviceName(ParameterList_t *TempParam);
int SetLocalClassOfDevice(ParameterList_t *TempParam);
int SetDiscoverable(ParameterList_t *TempParam);
int SetConnectable(ParameterList_t *TempParam);
int SetPairable(ParameterList_t *TempParam);
int StartDeviceDiscovery(ParameterList_t *TempParam);
int StopDeviceDiscovery(ParameterList_t *TempParam);
int QueryRemoteDeviceList(ParameterList_t *TempParam);
int QueryRemoteDeviceProperties(ParameterList_t *TempParam);
int AddRemoteDevice(ParameterList_t *TempParam);
int DeleteRemoteDevice(ParameterList_t *TempParam);
int DeleteRemoteDevices(ParameterList_t *TempParam);
int PairWithRemoteDevice(ParameterList_t *TempParam);
int CancelPairWithRemoteDevice(ParameterList_t *TempParam);
int UnPairRemoteDevice(ParameterList_t *TempParam);
int QueryRemoteDeviceServices(ParameterList_t *TempParam);
int RegisterAuthentication(ParameterList_t *TempParam);
int UnRegisterAuthentication(ParameterList_t *TempParam);

int ChangeSimplePairingParameters(ParameterList_t *TempParam);
int PINCodeResponse(ParameterList_t *TempParam);
int PassKeyResponse(ParameterList_t *TempParam);
int UserConfirmationResponse(ParameterList_t *TempParam);

int ManageAudioConnection(ParameterList_t *TempParam);
int HDSMSetupAudioConnection(BD_ADDR_t BD_ADDR, Boolean_t InBandRinging);
int HDSMReleaseAudioConnection(BD_ADDR_t BD_ADDR);
int HDSMConnectDevice(ParameterList_t *TempParam);
int HDSMDisconnectDevice(ParameterList_t *TempParam);
int HDSMConnectionRequestResponse(ParameterList_t *TempParam);
int HDSMSetRemoteSpeakerGain(ParameterList_t *TempParam);
int HDSMSetRemoteMicrophoneGain(ParameterList_t *TempParam);
int QueryHeadsetServices(ParameterList_t *TempParam);
int ChangeIncomingConnectionFlags(ParameterList_t *TempParam);

int HDSMRingIndication(ParameterList_t *TempParam);

int HDSRegisterEventCallback(ParameterList_t *TempParam);
int HDSUnRegisterEventCallback(ParameterList_t *TempParam);
int HDSRegisterDataCallback(ParameterList_t *TempParam);
int HDSUnRegisterDataCallback(ParameterList_t *TempParam);
/* The following function Sets the HDK platform flag like the -k option at start */
int EnableHDK(ParameterList_t *TempParam);
int HDSSendAudio(ParameterList_t *TempParam);

Word_t ExtractUUID_ID(SDP_Data_Element_t *DataElementPtr);
Boolean_t ParseAudioGatewayServiceClass(SDP_Record_t *SDPRecordPtr);
Boolean_t ProcessHeadsetServiceRecord(SDP_Record_t *SDPRecordPtr, HDS_Profile_Info_t *ProfileInfoPtr);
Boolean_t ParseHeadsetSDPAttributeResponse(SDP_Service_Attribute_Response_Data_t *SDPServiceAttributeResponse, HDS_Profile_Info_t *ProfileInfo);

int GetHeadsetServiceInformation(BD_ADDR_t BD_ADDR, Boolean_t ForceUpdate, unsigned int *PortNumber);
Boolean_t ParseHeadsetData(DEVM_Parsed_SDP_Data_t *ParsedSDPData, HDS_Profile_Info_t *ProfileInfo);
void DisplayParsedServiceData(DEVM_Parsed_SDP_Data_t *ParsedSDPData);
void DisplaySDPAttributeResponse(SDP_Service_Attribute_Response_Data_t *SDPServiceAttributeResponse, unsigned int InitLevel);
void DisplayDataElement(SDP_Data_Element_t *SDPDataElement, unsigned int Level);

void DisplayLocalDeviceProperties(unsigned long UpdateMask, DEVM_Local_Device_Properties_t *LocalDeviceProperties);
void DisplayRemoteDeviceProperties(unsigned long UpdateMask, DEVM_Remote_Device_Properties_t *RemoteDeviceProperties);

/* BTPM Server Un-Registration Callback function prototype.          */
void BTPSAPI ServerUnRegistrationCallback(void *CallbackParameter);

/* BTPM Local Device Manager Callback function prototype.            */
void BTPSAPI DEVM_Event_Callback(DEVM_Event_Data_t *EventData, void *CallbackParameter);

/* BTPM Local Device Manager Authentication Callback function        */
/* prototype.                                                        */
void BTPSAPI DEVM_Authentication_Callback(DEVM_Authentication_Information_t *AuthenticationRequestInformation, void *CallbackParameter);

/* BTPM HDS Manager Callback function prototype.                     */
void BTPSAPI HDSM_Event_Callback(HDSM_Event_Data_t *EventData, void *CallbackParameter);

/* Shell commands to activate the GStreamer in 16KHz */
#define SAMPLE_RATE_16KHZ   				(16)
#define ROUTE_GSTREAMER_WL_TO_AM335_SPK_16K "gst-launch-1.0 alsasrc device=plughw:WILINK8BT ! audio/x-raw,channels=1,rate=16000 ! alsasink device=plughw:AM335xEVM &"
#define ROUTE_GSTREAMER_CMD_SLEEP_1SEC		"sleep 1"
#define ROUTE_GSTREAMER_AM335_MIC_TO_WL_16K	"gst-launch-1.0 alsasrc device=plughw:AM335xEVM ! audio/x-raw,channels=2,rate=16000 ! alsasink device=plughw:WILINK8BT &"
/* Shell commands to activate the GStreamer in 8KHz */
#define SAMPLE_RATE_8KHZ    				(8)
#define ROUTE_GSTREAMER_WL_TO_AM335_SPK_8K 	"gst-launch-1.0 alsasrc device=plughw:WILINK8BT ! audio/x-raw,channels=1,rate=8000 ! alsasink device=plughw:AM335xEVM &"
#define ROUTE_GSTREAMER_AM335_MIC_TO_WL_8K 	"gst-launch-1.0 alsasrc device=plughw:AM335xEVM ! audio/x-raw,channels=2,rate=8000 ! alsasink device=plughw:WILINK8BT &"

#define KILL_GSTREAMER_PROCESS              "killall gst-launch-1.0"
/*
* The following function send shell commands to configure the platform
* Linux GSTreamer. The two paths that configured are:
* 1. From the board Audio LINE IN jack to the CODEC and then PCM data
* 	  to the WiLink PCM lines input. In normal operation that audio data
* 	  is the Bluetooth audio data in the air.
* 2. From the WiLink PCM output to the CODEC and then to the board HEADPHONES
*    jack. In normal operation that audio data is coming from Bluetooth.
*
*  The code below first send the routing for path 1 and then waits 2 sec
*  and route path 2.
*  If the GStreamer already active- kill the process before running the application
*  with the shell command: # killall gst-launch-1.0
*
*  For porting to other boards the audio devices added to the AM335x-EVM- WILINK8BT and
*  AM335xEVM, need to be configured for the new platform with the PCM lines.
*  Also the user can change the definitions above in order to change GStreamer parameters
*  like rate and more.
*  For changing the PCM Audio volume the user can send the shell commands:
*  Volume up in 3dB
*  # amixer -c 1 set PCM 3dB+
*  Volume down in 3dB
*  # amixer -c 1 set PCM 3dB-
*
*/
void RouteGStreamer(int freq);
/* The following function routes the platform Audio according the compiled device name */
void RouteAudio(void);
/* Parse the user arguments at start */
void ParseCmdLine(int argc, char *argv[], char **devname, unsigned long *flags);
/* Initialize the application with the start commands according the user input */
int HSP_AG_Init(char *devname, unsigned long flags);


int BTPSAPI write_SCO_Configuration(ParameterList_t *TempParam);


#endif // BLUETOPIA_H
